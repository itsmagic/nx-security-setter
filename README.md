NX Security Setter
============
NX Security Setter is a program that integrates unit discovery and telnet commands to ease configuration and management of AMX NX Controllers.

### For the Windows installer checkout our release page here:
**https://magicsoftware.ornear.com/updates/nx_security_setter/**
