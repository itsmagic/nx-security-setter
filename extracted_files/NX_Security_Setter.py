"""Configurator is a program that integrates device discovery and telnet
commands to ease configuration and management of AMX DXLink devices.

The MIT License (MIT)

Copyright (c) 2014 Jim Maciejewski

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE."""


import ConfigParser
import wx
import sys
import pickle
import datetime
import os
import csv
from ObjectListView import ObjectListView, ColumnDefn
import Queue
import webbrowser
import urllib
import requests
from distutils.version import StrictVersion
import re
from threading import Thread
from pydispatch import dispatcher

from scripts import (config_menus, dhcp_sniffer, nss_gui,
                     multi_ping, telnet_class, telnetto_class, websocket_class)


class Unit(object):
    """
    Model of the Unit

    Contains the following attributes:
    model, hostname, serial ,firmware, mac, ip, time, ip_type, gateway,
    subnet
    """
    def __init__(self,  model='', hostname='', serial='', firmware='',
                 mac='', ip_ad='', arrival_time='', ip_type='',
                 gateway='', subnet='', status=''):

        self.model = model
        self.hostname = hostname
        self.serial = serial
        self.firmware = firmware
        self.mac_address = mac
        self.ip_address = ip_ad
        self.arrival_time = arrival_time
        self.ip_type = ip_type
        self.gateway = gateway
        self.subnet = subnet
        self.status = status

        # Structure for security settings


class MainFrame(nss_gui.MainFrame):
    def __init__(self, parent):
        nss_gui.MainFrame.__init__(self, parent)

        self.parent = parent
        self.name = "NX Security Setter"
        self.version = "v0.1.1"

        icon_bundle = wx.IconBundle()
        icon_bundle.AddIconFromFile(r"icon\\NSS.ico", wx.BITMAP_TYPE_ANY)
        self.SetIcons(icon_bundle)
        self.columns_default = (
            'Model, ' +
            'Hostname, ' +
            'Firmware, ' +
            'Static, ' +
            'MAC, ' +
            'Serial')

        self.default_connection_type = None
        self.default_dhcp = None
        self.thread_number = None
        self.telnet_client = None
        self.telnet_timeout_seconds = None
        self.dhcp_sniffing = None
        self.amx_only_filter = None
        self.play_sounds = None
        self.username = ''
        self.password = ''
        self.columns_config = []
        self.config_fail = False
        self.telnet_missing = False
        self.debug_enable = False
        self.path = os.path.expanduser(
                '~\\Documents\\NX_Security_Setter\\')
        self.read_config_file()
        self.check_for_telnet_client()
        self.set_title_bar()
        self.port_error = False
        self.ping_objects = []
        self.ping_active = False
        self.ping_window = None

        self.main_list = ObjectListView(self.olv_panel, wx.ID_ANY,
                                        style=wx.LC_REPORT | wx.SUNKEN_BORDER)
        self.main_list.cellEditMode = ObjectListView.CELLEDIT_DOUBLECLICK

        # self.main_list.evenRowsBackColor = 'LIGHT GREY'
        # self.main_list.oddRowsBackColor = 'WHEAT'
        self.main_list.Bind(wx.EVT_LIST_ITEM_RIGHT_CLICK,
                            self.MainFrameOnContextMenu)
        self.main_list.Bind(wx.EVT_KEY_DOWN, self.on_key_down)

        self.columns = []
        self.min_panel_width = 450
        self.panel_width_offset = 60
        self.col_width = {
            'Time': 110,
            'Model': 160,
            'MAC': 125,
            'IP': 120,
            'Hostname': 150,
            'Serial': 150,
            'Firmware': 80,
            'Static': 50,
            'Status': 150
        }
        self.columns_setup = [
            ColumnDefn("Time", "center", self.col_width['Time'],
                       "arrival_time", stringConverter="%I:%M:%S%p"),
            ColumnDefn("Model", "left", self.col_width['Model'], "model"),
            ColumnDefn("MAC", "left", self.col_width['MAC'], "mac_address"),
            ColumnDefn("IP", "left", self.col_width['IP'], "ip_address"),
            ColumnDefn("Hostname", "left", self.col_width['Hostname'],
                       "hostname"),
            ColumnDefn("Serial", "left", self.col_width['Serial'], "serial"),
            ColumnDefn("Firmware", "left", self.col_width['Firmware'],
                       "firmware"),
            ColumnDefn("Static", "left", self.col_width['Static'], "ip_type"),
            ColumnDefn("Status", "left", self.col_width['Status'], "status")]

        self.select_columns()
        self.amx_only_filter_chk.Check(self.amx_only_filter)
        self.dhcp_sniffing_chk.Check(self.dhcp_sniffing)
        self.load_data_pickle()
        # self.update_status_bar()

        self.olv_sizer.Add(self.main_list, 1, wx.ALL | wx.EXPAND, 0)
        self.olv_sizer.Layout()
        self.resize_frame()

        # Create DHCP listening thread
        self.dhcp_listener = dhcp_sniffer.DHCPListener(self)
        self.dhcp_listener.setDaemon(True)
        self.dhcp_listener.start()

        # create a telenetto thread pool and assign them to a queue
        self.telnet_to_queue = Queue.Queue()
        for _ in range(10):
            self.telnet_to_thread = telnetto_class.TelnetToThread(
                self, self.telnet_to_queue)
            self.telnet_to_thread.setDaemon(True)
            self.telnet_to_thread.start()

        # create a telnetjob thread pool and assign them to a queue
        self.telnet_job_queue = Queue.Queue()
        for _ in range(int(self.thread_number)):
            self.telnet_job_thread = telnet_class.Telnetjobs(
                self, self.telnet_job_queue)
            self.telnet_job_thread.setDaemon(True)
            self.telnet_job_thread.start()

        # create a telnetjob thread pool and assign them to a queue
        self.websocket_job_queue = Queue.Queue()
        for _ in range(int(self.thread_number)):
            self.websocket_job_thread = websocket_class.Websocketjobs(
                self, self.websocket_job_queue)
            self.websocket_job_thread.setDaemon(True)
            self.websocket_job_thread.start()

        dispatcher.connect(self.incoming_packet,
                           signal="Incoming Packet",
                           sender=dispatcher.Any)
        dispatcher.connect(self.set_status,
                           signal="Status Update",
                           sender=dispatcher.Any)
        dispatcher.connect(self.status_messages,
                           signal="Status Message",
                           sender=dispatcher.Any)
        # dispatcher.connect(self.collect_errors,
        #                    signal="Collect Errors",
        #                    sender=dispatcher.Any)
        self.dhcp_listener.dhcp_sniffing_enabled = self.dhcp_sniffing
        if self.check_for_updates:
            Thread(target=self.update_check).start()

    def resource_path(self, relative):
        return os.path.join(getattr(sys, '_MEIPASS', os.path.abspath(".")),
                            relative)

    def update_check(self):
        """Checks on line for updates"""
        # print 'in update'
        try:
            update_url = 'http://magicsoftware.ornear.com/current_version/nx_security_setter/nss_current_version.txt'
            webpage = requests.get(update_url)
            # verify=self.cert_path)
            # print webpage.text
            # Scrape page for latest version
            # soup = BeautifulSoup(webpage.text)
            # # Get the <div> sections in lable-latest
            # # print 'divs'
            # divs = soup.find_all("div", class_="release label-latest")
            # # Get the 'href' of the release
            # url_path = divs[0].find_all('a')[-3].get('href')
            # Get the 'verison' number
            online_version = webpage.text[1:]
            if StrictVersion(online_version) > StrictVersion(self.version[1:]):
                # Try update
                # print 'try update'
                self.do_update(update_url, online_version)
            else:
                # All up to date pass
                # print 'up to date'
                return
        except Exception as error:
            print "Error in update_check: ", error
            pass

    def do_update(self, url_path, online_version):
        """download and install"""
        # ask if they want to update
        dlg = wx.MessageDialog(
                parent=self,
                message='A new NX Security Setter is available v' +
                        str(StrictVersion(online_version)) + '\r' +
                        'Do you want to download and update?',
                        caption='Do you want to update?',
                        style=wx.OK | wx.CANCEL)
        if dlg.ShowModal() == wx.ID_OK:
            response = requests.get('http://magicsoftware.ornear.com/software/nx_security_setter/' + 'NX_Security_Setter_' +
                                    str(StrictVersion(online_version)) + '.exe', stream=True)
            # print response
            if not response.ok:
                return
            total_length = response.headers.get('content-length')
            if total_length is None:  # no content length header
                pass
            else:
                total_length = int(total_length)
                dlg2 = wx.ProgressDialog("Download Progress",
                                         "Downloading update now",
                                         maximum=total_length,
                                         parent=self,
                                         style=wx.PD_APP_MODAL |
                                         wx.PD_AUTO_HIDE |
                                         wx.PD_CAN_ABORT |
                                         wx.PD_ELAPSED_TIME)
                temp_folder = os.environ.get('temp')
                with open(temp_folder +
                          '\NX_Security_Setter_' +
                          str(StrictVersion(online_version)) +
                          '.exe', 'wb') as handle:

                    count = 0
                    for data in response.iter_content(1024):
                        count += len(data)
                        handle.write(data)
                        (cancel, skip) = dlg2.Update(count)
                        if not cancel:
                            break

                # print 'out of for loop'
                dlg2.Destroy()
            if not cancel:
                return
            self.install_update(online_version, temp_folder)

    def install_update(self, online_version, temp_folder):
        """Installs the downloaded update"""
        dlg = wx.MessageDialog(
            parent=self,
            message='Do you want to update to version ' +
                    str(StrictVersion(online_version)) + ' now?',
            caption='Update program',
            style=wx.OK | wx.CANCEL)

        if dlg.ShowModal() == wx.ID_OK:
            os.startfile(temp_folder +
                         '\NX_Security_Setter_' +
                         str(StrictVersion(online_version)) +
                         '.exe')
            self.on_close(None)

    def on_key_down(self, event):
        """Grab Delete key presses"""
        key = event.GetKeyCode()
        if key == wx.WXK_DELETE:
            dlg = wx.MessageDialog(
                parent=self,
                message='Are you sure? \n\nThis will delete all selected ' +
                        'items in the list',
                caption='Delete All Selected Items',
                style=wx.OK | wx.CANCEL)

            if dlg.ShowModal() == wx.ID_OK:
                self.delete_item(None)
                self.dump_pickle()
            else:
                return
        event.Skip()

    def play_sound(self):
        """Plays a barking sound"""
        if self.play_sounds:
            filename = "sounds\\woof.wav"
            sound = wx.Sound(filename)
            if sound.IsOk():
                sound.Play(wx.SOUND_ASYNC)
            else:
                wx.MessageBox("Invalid sound file", "Error")

    def status_messages(self, sender):
        """Shows a status message"""
        try:
            if not self.show_script_messages:
                return
            clean_status = re.sub('<[^>]+>', '', sender[1]['device'])
            dlg = wx.MessageDialog(parent=self, message=clean_status,
                                   caption='Status Message',
                                   style=wx.OK)
            dlg.ShowModal()
        except Exception as error:
            print 'Error trying to show status message: ', error

    def set_status(self, sender):
        """sets the status of an object from a tuple of (obj, status)"""

        sender[0].status = sender[1]
        self.main_list.RefreshObject(sender[0])

    def port_errors(self):
        """Shows when the listening port is in use"""
        dlg = wx.MessageDialog(
            self,
            message='Unable to use port 67\n No DHCP requests will be added.',
            caption='Port in use',
            style=wx.ICON_INFORMATION)
        dlg.ShowModal()
        self.dhcp_sniffing_chk.Enable(False)
        self.amx_only_filter_chk.Enable(False)

    def on_dhcp_sniffing(self, _):
        """Turns sniffing on and off"""
        self.dhcp_sniffing = not self.dhcp_sniffing
        self.dhcp_sniffing_chk.Check(self.dhcp_sniffing)
        self.dhcp_listener.dhcp_sniffing_enabled = self.dhcp_sniffing
        self.write_config_file()

    def on_amx_only_filter(self, _):
        """Turns amx filtering on and off"""
        self.amx_only_filter = not self.amx_only_filter
        self.amx_only_filter_chk.Check(self.amx_only_filter)
        self.write_config_file()

    def select_columns(self):
        """Sets the columns to be displayed"""
        columns = self.columns_config + ['Time', 'IP', 'Status']
        todisplay = []
        for item in self.columns_setup:
            if item.title in columns:
                todisplay.append(item)
        self.main_list.SetColumns(todisplay)

    def check_for_none_selected(self):
        """Checks if nothing is selected"""
        if len(self.main_list.GetSelectedObjects()) == 0:
            dlg = wx.MessageDialog(
                parent=self, message='Nothing selected...\nPlease click on ' +
                'the device you want to select',
                caption='Nothing Selected',
                style=wx.OK)
            dlg.ShowModal()
            dlg.Destroy()
            return True
        for obj in self.main_list.GetSelectedObjects():
            self.set_status((obj, ''))

    def on_select_all(self, _):
        """Select all items in the list"""
        self.main_list.SelectAll()

    def on_select_none(self, _):
        """Select none of the items in the list"""
        self.main_list.DeselectAll()

    def config_fail_dia(self):
        """show config fail"""
        dlg = wx.MessageDialog(
            parent=self,
            message='New setting file created \n\n A new settings file was' +
                    ' created, \nbecause the old one couldn\'t ' +
                    'be read \nor was from a old version',
            caption='Default settings file created',
            style=wx.OK)

        dlg.ShowModal()

    def telnet_to(self, _):
        """Telnet to the selected device(s)"""
        if self.check_for_none_selected():
            return
        if len(self.main_list.GetSelectedObjects()) > 10:
            dlg = wx.MessageDialog(
                parent=self,
                message='I can only telnet to 10 devices at a time \nPlease ' +
                'select less than ten devices at once',
                caption='How many telnets?',
                style=wx.OK)
            dlg.ShowModal()
            dlg.Destroy()
            return

        if os.path.exists((self.path + self.telnet_client)):

            for obj in self.main_list.GetSelectedObjects():
                self.telnet_to_queue.put([obj, 'telnet'])
                self.set_status((obj, "Queued"))
        else:
            dlg = wx.MessageDialog(
                parent=self,
                message='Could not find ' +
                        'telnet client \nPlease put ' +
                        '%s in \n%s' % (self.telnet_client, self.path),
                caption='No %s' % self.telnet_client,
                style=wx.OK)
            dlg.ShowModal()
            dlg.Destroy()

    def ssh_to(self, _):
        """Telnet to the selected device(s)"""
        if self.check_for_none_selected():
            return
        if len(self.main_list.GetSelectedObjects()) > 10:
            dlg = wx.MessageDialog(
                parent=self, message='I can only ssh to' +
                ' 10 devices at a time \nPlease select less' +
                ' than ten devices at once',
                caption='How many ssh?',
                style=wx.OK)
            dlg.ShowModal()
            dlg.Destroy()
            return
        if os.path.exists((self.path + self.telnet_client)):

            for obj in self.main_list.GetSelectedObjects():
                self.telnet_to_queue.put([obj, 'ssh'])
                self.set_status((obj, "Queued"))
        else:
            dlg = wx.MessageDialog(
                parent=self,
                message='Could not find ssh client \nPlease put ' +
                '%s in \n%s' % (self.telnet_client, self.path),
                caption='No %s' % self.telnet_client,
                style=wx.OK)
            dlg.ShowModal()
            dlg.Destroy()
        return

    def multi_ping(self, _):
        """Ping and track results of many devices"""
        if self.check_for_none_selected():
            return
        if self.ping_active:
            dlg = wx.MessageDialog(parent=self,
                                   message='You already have a ' +
                                           'ping window open',
                                   caption='Are you crazy?',
                                   style=wx.OK)
            dlg.ShowModal()
            dlg.Destroy()
            return
        if len(self.main_list.GetSelectedObjects()) > int(self.thread_number):
            dlg = wx.MessageDialog(parent=self, message='I can only ping ' +
                                   self.thread_number +
                                   ' devices at a time \nPlease select less ' +
                                   'than ' + self.thread_number +
                                   ' devices at once',
                                   caption='How many pings?',
                                   style=wx.OK)
            dlg.ShowModal()
            dlg.Destroy()
            return

        self.ping_active = True
        self.ping_window = multi_ping.MultiPing(
            self, self.main_list.GetSelectedObjects())
        self.ping_window.Show()

    def custom_websocket_script(self, _):
        """Do some websocket stuff in a script"""
        if self.check_for_none_selected():
            return
        open_file_dialog = wx.FileDialog(self, message="Load Custom Script",
                                         defaultDir=self.path,
                                         defaultFile="",
                                         wildcard="Text files (*.txt)|*.txt",
                                         style=wx.FD_OPEN |
                                         wx.FD_FILE_MUST_EXIST)
        if open_file_dialog.ShowModal() == wx.ID_OK:
            open_file_dialog.Destroy()
        else:
            open_file_dialog.Destroy()
            return
        for obj in self.main_list.GetSelectedObjects():
            self.websocket_job_queue.put(['custom_websocket_script', obj, open_file_dialog.GetPath()])
            self.set_status((obj, "Queued"))

    def load_certificate(self, _):
        """Load ldap ceritifcate on server"""
        if self.check_for_none_selected():
            return
        open_file_dialog = wx.FileDialog(self, message="Load LDAP certificate",
                                         defaultDir=self.path,
                                         defaultFile="",
                                         wildcard="PEM files (*.pem)|*.pem",
                                         style=wx.FD_OPEN |
                                         wx.FD_FILE_MUST_EXIST)
        if open_file_dialog.ShowModal() == wx.ID_OK:
            open_file_dialog.Destroy()
        else:
            open_file_dialog.Destroy()
            return
        for obj in self.main_list.GetSelectedObjects():
            self.websocket_job_queue.put(['send_cert', obj, open_file_dialog.GetPath()])
            self.set_status((obj, "Queued"))

    def reset_factory(self, _):
        """Reset device to factory defaults"""
        if self.check_for_none_selected():
            return
        for obj in self.main_list.GetSelectedObjects():
            dlg = wx.MessageDialog(parent=self, message='Are you sure? \n ' +
                                   'This will reset %s' % obj.ip_address,
                                   caption='Factory Reset',
                                   style=wx.OK | wx.CANCEL)
            if dlg.ShowModal() == wx.ID_OK:
                dlg.Destroy()
                self.telnet_job_queue.put(['reset_factory', obj,
                                           self.telnet_timeout_seconds])
                self.set_status((obj, "Queued"))
            else:
                return

    def reboot(self, _):
        """Reboots device"""
        if self.check_for_none_selected():
            return
        for obj in self.main_list.GetSelectedObjects():
            self.telnet_job_queue.put(['reboot', obj,
                                       self.telnet_timeout_seconds])
            self.set_status((obj, "Queued"))

    def open_url(self, _):
        """Opens ip address in a browser"""
        if self.check_for_none_selected():
            return
        for obj in self.main_list.GetSelectedObjects():
            url = 'http://' + obj.ip_address
            webbrowser.open_new_tab(url)

    def update_device_information(self, _):
        """Connects to device via telnet and gets serial model and firmware """
        if self.check_for_none_selected():
            return
        for obj in self.main_list.GetSelectedObjects():
            self.telnet_job_queue.put(['get_config_info', obj,
                                       self.telnet_timeout_seconds])
            self.set_status((obj, "Queued"))

    def dump_pickle(self):
        """Saves list data to a file"""
        pickle.dump(self.main_list.GetObjects(), open(
            (self.path + 'data_' + self.version + '.pkl'), 'wb'))

    def export_to_csv(self, _):
        """Store list items in a CSV file"""
        if self.check_for_none_selected():
            return
        save_file_dialog = wx.FileDialog(
            self,
            message='Select file to add devices to or create a new file',
            defaultDir=self.path,
            defaultFile="",
            wildcard="CSV files (*.csv)|*.csv",
            style=wx.SAVE)
        if save_file_dialog.ShowModal() == wx.ID_OK:
            path = save_file_dialog.GetPath()
            try:
                with open(path, 'ab') as store_file:
                    write_csv = csv.writer(store_file, quoting=csv.QUOTE_ALL)
                    for obj in self.main_list.GetSelectedObjects():
                        data = [obj.model,
                                obj.hostname,
                                obj.serial,
                                obj.firmware,
                                obj.mac_address,
                                obj.ip_address,
                                obj.arrival_time,
                                obj.ip_type,
                                obj.gateway,
                                obj.subnet]
                        write_csv.writerow(data)
                        self.set_status((obj, 'Exported'))
            except Exception as error:
                dlg = wx.MessageDialog(parent=self, message='An error occurred: \n ' +
                                       str(error),
                                       caption='Error occurred exporting',
                                       style=wx.OK)
                dlg.ShowModal()
            self.dump_pickle()

    def import_csv_file(self, _):
        """Imports a list of devices to the main list"""
        open_file_dialog = wx.FileDialog(self, message="Import a CSV file",
                                         defaultDir=self.path,
                                         defaultFile="",
                                         wildcard="CSV files (*.csv)|*.csv",
                                         style=wx.FD_OPEN |
                                         wx.FD_FILE_MUST_EXIST)
        if open_file_dialog.ShowModal() == wx.ID_OK:
            open_file_dialog.Destroy()
            dlg = wx.MessageDialog(parent=self, message='To replace ' +
                                   'all items currently in your list,  ' +
                                   'click ok',
                                   caption='Replace items',
                                   style=wx.OK | wx.CANCEL)
            if dlg.ShowModal() == wx.ID_OK:
                self.main_list.DeleteAllItems()

            with open(open_file_dialog.GetPath(), 'rb') as csvfile:
                cvs_data = csv.reader(csvfile)
                for item in cvs_data:
                    data = Unit(
                        model=item[0],
                        hostname=item[1],
                        serial=item[2],
                        firmware=item[3],
                        mac=item[4],
                        ip_ad=item[5],
                        arrival_time=datetime.datetime.strptime(
                            (item[6]), "%Y-%m-%d %H:%M:%S.%f"),
                        ip_type=item[7],
                        gateway=item[8],
                        subnet=item[9])

                    self.main_list.AddObject(data)
            self.dump_pickle()

        else:
            open_file_dialog.Destroy()

    def import_ip_list(self, _):
        """Imports a list of IP addresses"""
        open_file_dialog = wx.FileDialog(
            self, message="Open IP List",
            defaultDir=self.path,
            defaultFile="",
            wildcard="CSV files (*.csv)|*.csv",
            style=wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)

        if open_file_dialog.ShowModal() == wx.ID_OK:
            self.main_list.DeleteAllItems()
            with open(open_file_dialog.GetPath(), 'rb') as csvfile:
                cvs_data = csv.reader(csvfile)
                for item in cvs_data:
                    data = Unit(
                        ip_ad=item[0],
                        arrival_time=datetime.datetime.now())

                    self.main_list.AddObject(data)
            self.dump_pickle()
            open_file_dialog.Destroy()
        else:
            open_file_dialog.Destroy()

    def generate_list(self, _):
        """Generates a list of ip addresses"""
        dia = config_menus.IpListGen(self)
        dia.ShowModal()
        dia.Destroy()

    def add_line(self, _):
        """Adds a line to the main list"""
        data = Unit(arrival_time=datetime.datetime.now())
        self.main_list.AddObject(data)
        self.dump_pickle()

    def delete_item(self, _):
        """Deletes the selected item"""
        if self.check_for_none_selected():
            return
        if len(self.main_list.GetSelectedObjects()) == \
           len(self.main_list.GetObjects()):
            self.main_list.DeleteAllItems()
            self.dump_pickle()
            return
        self.main_list.RemoveObjects(self.main_list.GetSelectedObjects())
        self.dump_pickle()

    def delete_all_items(self, _):
        """Deletes all items,selected or not"""
        dlg = wx.MessageDialog(parent=self, message='Are you sure? \n This ' +
                               'will delete all items in the list',
                               caption='Delete All Items',
                               style=wx.OK | wx.CANCEL)
        if dlg.ShowModal() == wx.ID_OK:
            self.main_list.DeleteAllItems()
            self.dump_pickle()
        else:
            return

    def create_add_unit(self, model='', hostname='', serial='', firmware='',
                        mac='', ip_ad='', arrival_time='',
                        ip_type='', gateway='', subnet=''):
        """Creates and adds a unit"""
        data = Unit(
            model=model,
            hostname=hostname,
            serial=serial,
            firmware=firmware,
            mac=mac,
            ip_ad=ip_ad,
            arrival_time=datetime.datetime.now(),
            ip_type=ip_type,
            gateway=gateway,
            subnet=subnet)
        self.main_list.AddObject(data)
        return data

    def incoming_packet(self, sender):
        """Receives dhcp requests and adds them to objects to display"""
        incoming_time = datetime.datetime.now()
        data = Unit(
            hostname=sender[0],
            mac=sender[1],
            ip_ad=sender[2],
            arrival_time=incoming_time)

        self.status_bar.SetStatusText(
            incoming_time.strftime('%I:%M:%S%p') +
            ' -- ' + data.hostname +
            ' ' + data.ip_address +
            ' ' + data.mac_address)

        if bool(self.amx_only_filter):
            if data.mac_address[0:8] != '00:60:9f':
                self.main_list.SetFocus()
                return
        selected_items = self.main_list.GetSelectedObjects()
        if self.main_list.GetObjects() == []:
            self.main_list.AddObject(data)
            self.set_status((data, "DHCP"))
        else:
            for obj in self.main_list.GetObjects():
                if obj.mac_address == data.mac_address:
                    if obj.arrival_time > (
                            incoming_time - datetime.timedelta(seconds=2)):
                        break
                    data.model = obj.model
                    data.serial = obj.serial
                    data.firmware = obj.firmware
                    data.ip_type = obj.ip_type
                    data.gateway = obj.gateway
                    data.subnet = obj.subnet
                    if obj in selected_items:
                        selected_items.append(data)
                    self.main_list.RemoveObject(obj)
            self.main_list.AddObject(data)

            self.set_status((data, "DHCP"))
        if data.hostname[:2] == 'DX':
            self.telnet_job_queue.put(['get_config_info', data,
                                       self.telnet_timeout_seconds])
        self.main_list.SelectObjects(selected_items, deselectOthers=True)
        self.dump_pickle()
        self.play_sound()

    def read_config_file(self):
        """Reads the config file"""
        config = ConfigParser.RawConfigParser()
        try:  # read the settings file
            config.read((self.path + "settings.txt"))
            self.thread_number = (config.get(
                'Settings', 'number of threads'))
            self.telnet_client = (config.get(
                'Settings', 'telnet client executable'))
            self.telnet_timeout_seconds = (config.get(
                'Settings', 'telnet timeout in seconds'))
            self.dhcp_sniffing = (config.getboolean(
                'Settings', 'DHCP sniffing enabled'))
            self.amx_only_filter = (config.getboolean(
                'Settings', 'filter incoming DHCP for AMX only'))
            self.debug_enable = (config.getboolean(
                'Settings', 'debug enable'))
            self.play_sounds = (config.getboolean(
                'Settings', 'play sounds'))
            self.check_for_updates = (config.getboolean(
                'Settings', 'check for updates'))
            self.show_script_messages = (config.getboolean(
                'Settings', 'show script messages'))
            self.username = (config.get(
                'Settings', 'username'))
            self.password = (config.get(
                'Settings', 'password'))
            # self.encrypted_password = (config.get(
            #     'Settings', 'encrypted_password'))
            self.columns_config = []
            for item in config.get(
                    'Config', 'columns_config').split(','):
                self.columns_config.append(item.strip())

        except:  # (ConfigParser.Error, IOError):
            # Make a new settings file, because we couldn't read the old one
            self.create_config_file()
            self.read_config_file()
        return

    def create_config_file(self):
        """Creates a new config file"""
        self.config_fail = True
        if not os.path.exists(self.path):
            os.makedirs(self.path)
        try:
            if os.path.exists(self.path + 'settings.txt'):
                os.remove(self.path + 'settings.txt')
        except OSError:
            pass
        with open((self.path + "settings.txt"), 'w') as config_file:
            config_file.write("")
        config = ConfigParser.RawConfigParser()
        config.add_section('Settings')
        config.set('Settings', 'number of threads', 20)
        config.set('Settings', 'telnet client executable', ('putty.exe'))
        config.set('Settings', 'telnet timeout in seconds', '4')
        config.set('Settings', 'DHCP sniffing enabled', False)
        config.set('Settings', 'filter incoming DHCP for AMX only', False)
        config.set('Settings', 'debug enable', False)
        config.set('Settings', 'play sounds', False)
        config.set('Settings', 'check for updates', True)
        config.set('Settings', 'show script messages', False)
        config.set('Settings', 'username', 'administrator')
        config.set('Settings', 'password', 'password')
        # config.set('Settings', 'encrypted_password',
        #                        'gAAAAABW3TPjilgSav0NmSrvZDziGeK5XfpSxtwIeAvS' +
        #                        'OXftcXwqJNZ3cF0WXiBPitHMdO2fCnqSIw_euSDmGVFh' +
        #                        'NtLGLuR9JnsmxGG8gkAajzN68_jaUgo=')
        config.add_section('Config')
        config.set('Config', 'columns_config', self.columns_default)
        with open((self.path + "settings.txt"), 'w') as configfile:
            config.write(configfile)

    def write_config_file(self):
        """Update values in config file"""
        config = ConfigParser.RawConfigParser()
        config.read((self.path + "settings.txt"))
        config.set('Settings', 'number of threads', self.thread_number)
        config.set('Settings', 'telnet client executable', self.telnet_client)
        config.set('Settings', 'telnet timeout in seconds',
                   self.telnet_timeout_seconds)
        config.set('Settings', 'DHCP sniffing enabled', self.dhcp_sniffing)
        config.set('Settings', 'filter incoming DHCP for AMX only',
                   self.amx_only_filter)
        config.set('Settings', 'debug enable', self.debug_enable)
        config.set('Settings', 'play sounds', self.play_sounds)
        config.set('Settings', 'check for updates', self.check_for_updates)
        config.set('Settings', 'show script messages', self.show_script_messages)
        config.set('Settings', 'username', self.username)
        config.set('Settings', 'password', self.password)
        # config.set('Settings', 'encrypted_password', self.encrypted_password)
        columns = ''
        for item in self.columns_config:
            columns = columns + item + ', '
        columns = columns[:-2]
        config.set('Config', 'columns_config', columns)
        with open((self.path + "settings.txt"), 'w') as configfile:
            config.write(configfile)

    def check_for_telnet_client(self):
        """Checks if telnet client in the path"""
        if not os.path.exists(self.path + self.telnet_client):
            self.telnet_missing = True

    def telnet_missing_dia(self):
        """Show dialog for missing putty"""
        dlg = wx.MessageDialog(
            parent=self,
            message='I notice that you don\'t have a copy of putty.exe \n' +
            'availiable. Click OK to automaticly download this \n' +
            'telnet client.',
            caption='Auto download',
            style=wx.OK | wx.CANCEL)
        if dlg.ShowModal() == wx.ID_OK:
            # download

            putty_url = ('http://the.earth.li/~sgtatham/' +
                         'putty/latest/x86/putty.exe')
            try:
                # resp = requests.head(putty_url)
                # if resp.status_code != 404:
                urllib.urlretrieve(putty_url, self.path + 'putty.exe')
                dlg.Destroy()
                return
            except Exception as error:
                print error
                dlg.Destroy()
        dlg = wx.MessageDialog(
            parent=self,
            message='Please manually copy ' +
                    self.telnet_client +
                    ' to: \n' +
                    self.path +
                    ' \nThis will allow you to telnet to a device.',
            caption='No telnet client',
            style=wx.OK)

        dlg.ShowModal()
        dlg.Destroy()

    def set_title_bar(self):
        """Sets title bar text"""
        self.SetTitle(self.name + " " + self.version)

    def configure_prefs(self, _):
        """Sets user Preferences"""
        dia = config_menus.PreferencesConfig(self)
        dia.Show()

    def load_data_pickle(self):
        """Loads main list from data file"""
        if os.path.exists(self.path + 'data_' + self.version + '.pkl'):
            try:
                with open((self.path + 'data_' + self.version + '.pkl'),
                          'rb') as data_file:
                    objects = pickle.load(data_file)
                    self.main_list.SetObjects(objects)
            except (IOError, KeyError):
                self.new_pickle()
        self.main_list.SetSortColumn(0, resortNow=True)

    def new_pickle(self):
        """Creates a new pickle if there is a problem with the old one"""
        try:
            if os.path.exists((self.path + 'data_' + self.version + '.pkl')):
                os.rename(self.path + 'data_' + self.version + '.pkl',
                          self.path + 'data_' + self.version + '.bad')
        except IOError:
            dlg = wx.MessageDialog(parent=self, message='There is a problem ' +
                                   'with the .pkl data file. Please delete ' +
                                   'to continue. ' +
                                   ' The program will now exit',
                                   caption='Problem with .pkl file',
                                   style=wx.OK)
            dlg.ShowModal()
            self.Destroy()

    def resize_frame(self):
        """Resizes the Frame"""
        panel_width = self.panel_width_offset
        columns = ['Time', 'IP', 'Status']
        if self.columns_config != ['']:
            columns += self.columns_config
        for item in columns:
            panel_width += self.col_width[item]
        if panel_width < self.min_panel_width:
            panel_width = self.min_panel_width
        self.SetSize((panel_width, 600))

    def on_close(self, _):
        """Close program if user closes window"""
        self.dhcp_listener.shutdown = True
        self.ping_active = False
        self.dump_pickle()
        self.Hide()
        if self.ping_window is not None:
            self.ping_window.Hide()

        self.mse_active_list = []
        self.telnet_job_queue.join()
        self.Destroy()

    def on_quit(self, _):
        """Save list and close the program"""
        self.on_close(None)

    def on_about_box(self, _):
        """Show the About information"""

        description = """
Magic NX Security Setter is an tool for updating master
security settings. Features include a DHCP monitor,
import and export csv files, batch ip listing,
serial number extraction, reboots and more.
"""

        licence = """The MIT License (MIT)

Copyright (c) 2016 Jim Maciejewski

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE."""

        info = wx.AboutDialogInfo()
        info.SetName(self.name)
        info.SetVersion(self.version)
        info.SetDescription(description)
        info.SetLicence(licence)
        info.AddDeveloper('Jim Maciejewski')
        wx.AboutBox(info)

    def on_beer_box(self, _):
        """ Buy me a beer! Yea!"""
        dlg = wx.MessageDialog(parent=self, message='If you enjoy this ' +
                               'program \n Learn how you can help out',
                               caption='Buy me a beer',
                               style=wx.OK)
        if dlg.ShowModal() == wx.ID_OK:
            url = 'http://ornear.com/give_a_beer'
            webbrowser.open_new_tab(url)
        dlg.Destroy()


def main():
    """run the main program"""
    nss_app = wx.App(redirect=True, filename="log.txt")
    # do processing/initialization here and create main window
    nss_frame = MainFrame(None)
    nss_frame.Show()

    if nss_frame.config_fail is True:
        nss_frame.config_fail_dia()
    if nss_frame.telnet_missing is True:
        nss_frame.telnet_missing_dia()
    nss_app.MainLoop()

if __name__ == '__main__':
    main()

 