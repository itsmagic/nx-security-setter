# Embedded file name: scripts\dhcp_sniffer.py
"""A DCHP sniffer that listens for DHCP request messages"""
from threading import Thread
import socket
from pydispatch import dispatcher
import wx
import time

class DHCPListener(Thread):
    """The dhcp listener thread"""

    def __init__(self, parent):
        """Init Worker Thread Class."""
        self.listen = True
        self.parent = parent
        self.lis_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.dhcp_options = None
        self.dhcp_sniffing_enabled = False
        self.shutdown = False
        self.received = None
        self.last_received = 0
        self.last_mac = ''
        Thread.__init__(self)
        return

    def run(self):
        """Run Worker Thread."""
        try:
            port = 67
            self.lis_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            self.lis_sock.bind(('', port))
        except IOError:
            self.parent.portError = True

        self.last_received = time.time() - 30
        while not self.shutdown:
            msg, _ = self.lis_sock.recvfrom(1024)
            self.received = time.time()
            if msg[240] == '5' and msg[241] == '\x01' and msg[242] == '\x03':
                mac_address = msg[28].encode('hex') + ':' + msg[29].encode('hex') + ':' + msg[30].encode('hex') + ':' + msg[31].encode('hex') + ':' + msg[32].encode('hex') + ':' + msg[33].encode('hex')
                self.dhcp_options = msg[243:]
                ip_address = ''
                hostname = ''
                while self.dhcp_options:
                    opt = self.dhcp_options[0]
                    if opt == '\xff':
                        self.dump_byte()
                        break
                    if opt == '\x00':
                        self.dump_byte()
                        continue
                    if opt == '2':
                        ip_address = '.'.join((str(ord(c)) for c in self.read_data(self.get_to_data())))
                        continue
                    if opt == '\x0c':
                        hostname = ''.join((c for c in self.read_data(self.get_to_data())))
                        continue
                    self.read_data(self.get_to_data())

                if ip_address == '':
                    continue
                if self.last_received < self.received + 5 and self.last_mac == mac_address:
                    continue
                self.last_received = time.time()
                self.last_mac = mac_address
                if not self.shutdown and self.dhcp_sniffing_enabled:
                    wx.CallAfter(self.send_info, (hostname, mac_address, ip_address))

    def read_data(self, data_length):
        """Reads the data portion of the DHCP option"""
        read_data = []
        for _ in range(0, data_length):
            read_data.append(self.dhcp_options[0])
            self.dhcp_options = self.dhcp_options[1:]

        return read_data

    def dump_byte(self):
        """Move to the next byte"""
        self.dhcp_options = self.dhcp_options[1:]

    def get_to_data(self):
        """Read the data length and option number then move to the data"""
        self.dump_byte()
        data_length = ord(self.dhcp_options[0])
        self.dump_byte()
        return data_length

    def send_info(self, info):
        """
        Send data to GUI
        """
        dispatcher.send(signal='Incoming Packet', sender=info)