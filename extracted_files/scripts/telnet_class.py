# Embedded file name: scripts\telnet_class.py
""" Telnets to devices to preform tasks"""
import telnetlib
from pydispatch import dispatcher
from threading import Thread
import datetime
import subprocess
from netifaces import interfaces, ifaddresses, AF_INET

class Telnetjobs(Thread):

    def __init__(self, parent, queue):
        Thread.__init__(self)
        self.queue = queue
        self.parent = parent
        self.server_running = False

    def run(self):
        while True:
            job = self.queue.get()
            getattr(self, job[0])(job)
            self.queue.task_done()

    def establish_telnet(self, ip_address, tel_port = 23):
        """Creates the telnet instance"""
        telnet_session = telnetlib.Telnet(ip_address, tel_port, 5)
        telnet_session.set_option_negotiation_callback(self.call_back)
        return telnet_session

    def call_back(self, sock, cmd, opt):
        """ Turns on server side echoing"""
        if opt == telnetlib.ECHO and cmd in (telnetlib.WILL, telnetlib.WONT):
            sock.sendall(telnetlib.IAC + telnetlib.DO + telnetlib.ECHO)

    def get_config_info(self, job):
        """Gets serial number, firmware from device"""
        obj = job[1]
        self.set_status(obj, 'Connecting')
        try:
            telnet_session = self.establish_telnet(obj.ip_address)
            need_to_login, intro = self.check_for_login(telnet_session)
            if need_to_login:
                intro = self.login(telnet_session, self.parent.username, self.parent.password)
                if intro is False:
                    self.set_status(obj, 'Login Failed')
                    return
            obj.model = intro[2]
            obj.firmware = intro[3]
            telnet_session.write('show system \r')
            telnet_session.read_until('Serial=', int(job[2]))
            obj.serial = telnet_session.read_until(',', int(job[2])).split("'")[1]
            telnet_session.write('get ip \r')
            telnet_session.read_until('HostName', int(job[2]))
            ip_host = telnet_session.read_until('Type').split()
            if len(ip_host) == 1:
                obj.hostname = ''
            else:
                obj.hostname = ' '.join(ip_host[:-1])
            ip_type = telnet_session.read_until('IP').split()
            if ip_type[0] == 'Static':
                obj.ip_type = 's'
            if ip_type[0] == 'DHCP':
                obj.ip_type = 'd'
            telnet_session.read_until('Mask')
            ip_subnet = telnet_session.read_until('IP').split()
            obj.subnet = ip_subnet[-2]
            telnet_session.read_until('IP')
            ip_gateway = telnet_session.read_until('IP').split()
            obj.gateway = ip_gateway[-2]
            telnet_session.read_until('MAC Address')
            ip_mac = telnet_session.read_until('Lease', int(job[2])).split()
            obj.mac_address = ip_mac[0]
            telnet_session.write('exit')
            telnet_session.close()
            self.set_status(obj, 'Success')
        except (IOError, Exception) as error:
            self.error_processing(obj, error)

    def check_for_login(self, telnet_session):
        """Checks if we need to login and returns the header"""
        intro = telnet_session.read_some()
        if intro.split()[0] == 'Login':
            need_to_login = True
        else:
            need_to_login = False
        return (need_to_login, intro.split())

    def login(self, telnet_session, username, password):
        """Log in when required"""
        try:
            telnet_session.write(username + '\r')
            telnet_session.read_until('Password :')
            telnet_session.write(password + '\r')
            intro = telnet_session.read_until('>', 5).split()[1:]
            if intro[0] != 'Welcome':
                return False
            return intro
        except Exception as error:
            print 'during login: ', error
            return False

    def ip4_addresses(self):
        ip_list = []
        for interface in interfaces():
            for link in ifaddresses(interface)[AF_INET]:
                ip_list.append(link['addr'])

        return ip_list

    def reset_factory(self, job):
        """Sets unit to factory defaults"""
        obj = job[1]
        self.set_status(obj, 'Connecting')
        try:
            telnet_session = self.establish_telnet(obj.ip_address)
            need_to_login, intro = self.check_for_login(telnet_session)
            if need_to_login:
                intro = self.login(telnet_session, self.parent.username, self.parent.password)
                if intro is False:
                    self.set_status(obj, 'Login Failed')
                    return
            telnet_session.read_until('>', int(job[2]))
            telnet_session.write('reset factory\r')
            telnet_session.read_until('?', int(job[2]))
            telnet_session.write('y\r')
            telnet_session.close()
            self.set_status(obj, 'Success')
        except IOError as error:
            self.error_processing(obj, error)

    def reboot(self, job):
        obj = job[1]
        self.set_status(obj, 'Connecting')
        try:
            telnet_session = self.establish_telnet(obj.ip_address)
            need_to_login, intro = self.check_for_login(telnet_session)
            if need_to_login:
                intro = self.login(telnet_session, self.parent.username, self.parent.password)
                if intro is False:
                    self.set_status(obj, 'Login Failed')
                    return
            telnet_session.read_until('>', int(job[2]))
            telnet_session.write('reboot \r')
            telnet_session.read_until('Rebooting....', int(job[2]))
            telnet_session.close()
            self.set_status(obj, 'Success')
        except Exception as error:
            self.error_processing(obj, error)

    def ping(self, job):
        """Ping devices constantly for troubleshooting"""
        obj = job[1]
        startupinfo = subprocess.STARTUPINFO()
        startupinfo.dwFlags |= subprocess.STARTF_USESHOWWINDOW
        ping = subprocess.Popen(['ping', obj.ip_address, '-t'], shell=False, stdout=subprocess.PIPE, startupinfo=startupinfo)
        while self.parent.ping_active:
            for line in iter(ping.stdout.readline, ''):
                result = line.rstrip()
                if len(result) < 10:
                    continue
                if result == '':
                    continue
                elif result == '\n':
                    continue
                elif result[:7] == 'Pinging':
                    continue
                elif result.split()[-1] == 'unreachable.' or result == 'Request timed out.':
                    success = 'No'
                    ms_delay = 'N/A'
                    data = (obj, [datetime.datetime.now(), ms_delay, success])
                    if self.parent.ping_active:
                        dispatcher.send(signal='Incoming Ping', sender=data)
                elif result.split()[-1][:3] == 'TTL':
                    temp = result.split()[-2]
                    ms_delay = ''.join([ str(s) for s in temp if s.isdigit() ])
                    success = 'Yes'
                    data = (obj, [datetime.datetime.now(), ms_delay, success])
                    if self.parent.ping_active:
                        dispatcher.send(signal='Incoming Ping', sender=data)
                else:
                    success = 'No'
                    ms_delay = 'N/A'
                    data = (obj, [datetime.datetime.now(), ms_delay, success])
                    if self.parent.ping_active:
                        dispatcher.send(signal='Incoming Ping', sender=data)
                if not self.parent.ping_active:
                    break

        ping.kill()

    def set_status(self, obj, status):
        """Updates progress in main"""
        data = (obj, status)
        dispatcher.send(signal='Status Update', sender=data)

    def notify_send_command_window(self, obj):
        """updates send_command window"""
        dispatcher.send(signal='Update Window', sender=obj)

    def error_processing(self, obj, error):
        """Send notification of error to main"""
        if str(error) == 'Not an AMX device':
            data = (obj, 'Warning, not a recognized dxlink device')
        else:
            data = (obj, str(error))
        dispatcher.send(signal='Status Update', sender=data)