# Embedded file name: scripts\nss_gui.py
import wx
import wx.xrc

class MainFrame(wx.Frame):

    def __init__(self, parent):
        wx.Frame.__init__(self, parent, id=wx.ID_ANY, title=wx.EmptyString, pos=wx.DefaultPosition, size=wx.Size(876, 603), style=wx.DEFAULT_FRAME_STYLE | wx.TAB_TRAVERSAL)
        self.SetSizeHintsSz(wx.DefaultSize, wx.DefaultSize)
        bSizer1 = wx.BoxSizer(wx.VERTICAL)
        bSizer2 = wx.BoxSizer(wx.VERTICAL)
        self.olv_panel = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.olv_sizer = wx.BoxSizer(wx.VERTICAL)
        self.olv_panel.SetSizer(self.olv_sizer)
        self.olv_panel.Layout()
        self.olv_sizer.Fit(self.olv_panel)
        bSizer2.Add(self.olv_panel, 1, wx.EXPAND, 5)
        bSizer1.Add(bSizer2, 1, wx.EXPAND, 5)
        self.SetSizer(bSizer1)
        self.Layout()
        self.m_menubar1 = wx.MenuBar(0)
        self.file_menu = wx.Menu()
        self.import_menu = wx.Menu()
        self.import_csv_menu = wx.MenuItem(self.import_menu, wx.ID_ANY, u'Import from a CSV', wx.EmptyString, wx.ITEM_NORMAL)
        self.import_menu.AppendItem(self.import_csv_menu)
        self.import_ip_menu = wx.MenuItem(self.import_menu, wx.ID_ANY, u'Import IP list', wx.EmptyString, wx.ITEM_NORMAL)
        self.import_menu.AppendItem(self.import_ip_menu)
        self.file_menu.AppendSubMenu(self.import_menu, u'Import')
        self.export_menu = wx.Menu()
        self.export_csv_menu = wx.MenuItem(self.export_menu, wx.ID_ANY, u'Export to a CSV File', wx.EmptyString, wx.ITEM_NORMAL)
        self.export_menu.AppendItem(self.export_csv_menu)
        self.file_menu.AppendSubMenu(self.export_menu, u'Export')
        self.quit_menu = wx.MenuItem(self.file_menu, wx.ID_ANY, u'Quit', wx.EmptyString, wx.ITEM_NORMAL)
        self.file_menu.AppendItem(self.quit_menu)
        self.m_menubar1.Append(self.file_menu, u'File')
        self.edit_menu = wx.Menu()
        self.select_menu = wx.Menu()
        self.all_menu = wx.MenuItem(self.select_menu, wx.ID_ANY, u'Select All', wx.EmptyString, wx.ITEM_NORMAL)
        self.select_menu.AppendItem(self.all_menu)
        self.none_menu = wx.MenuItem(self.select_menu, wx.ID_ANY, u'Select None', wx.EmptyString, wx.ITEM_NORMAL)
        self.select_menu.AppendItem(self.none_menu)
        self.edit_menu.AppendSubMenu(self.select_menu, u'Select')
        self.preferences_menu = wx.MenuItem(self.edit_menu, wx.ID_ANY, u'Preferences', wx.EmptyString, wx.ITEM_NORMAL)
        self.edit_menu.AppendItem(self.preferences_menu)
        self.m_menubar1.Append(self.edit_menu, u'Edit')
        self.actions_menu = wx.Menu()
        self.update_device_menu = wx.MenuItem(self.actions_menu, wx.ID_ANY, u'Update Device Information', wx.EmptyString, wx.ITEM_NORMAL)
        self.actions_menu.AppendItem(self.update_device_menu)
        self.telnet_menu = wx.MenuItem(self.actions_menu, wx.ID_ANY, u'Telnet to Device', wx.EmptyString, wx.ITEM_NORMAL)
        self.actions_menu.AppendItem(self.telnet_menu)
        self.ssh_menu = wx.MenuItem(self.actions_menu, wx.ID_ANY, u'SSH to Device', wx.EmptyString, wx.ITEM_NORMAL)
        self.actions_menu.AppendItem(self.ssh_menu)
        self.reset_menu = wx.MenuItem(self.actions_menu, wx.ID_ANY, u'Reset Factory', wx.EmptyString, wx.ITEM_NORMAL)
        self.actions_menu.AppendItem(self.reset_menu)
        self.reboot_menu = wx.MenuItem(self.actions_menu, wx.ID_ANY, u'Reboot Device', wx.EmptyString, wx.ITEM_NORMAL)
        self.actions_menu.AppendItem(self.reboot_menu)
        self.m_menubar1.Append(self.actions_menu, u'Actions')
        self.tools_menu = wx.Menu()
        self.ping_menu = wx.MenuItem(self.tools_menu, wx.ID_ANY, u'Ping Devices', wx.EmptyString, wx.ITEM_NORMAL)
        self.tools_menu.AppendItem(self.ping_menu)
        self.add_menu = wx.MenuItem(self.tools_menu, wx.ID_ANY, u'Add line item', wx.EmptyString, wx.ITEM_NORMAL)
        self.tools_menu.AppendItem(self.add_menu)
        self.generate_menu = wx.MenuItem(self.tools_menu, wx.ID_ANY, u'Generate IP List', wx.EmptyString, wx.ITEM_NORMAL)
        self.tools_menu.AppendItem(self.generate_menu)
        self.cert_menu = wx.MenuItem(self.tools_menu, wx.ID_ANY, u'FTP CA Certificate', wx.EmptyString, wx.ITEM_NORMAL)
        self.tools_menu.AppendItem(self.cert_menu)
        self.m_menubar1.Append(self.tools_menu, u'Tools')
        self.listen_menu = wx.Menu()
        self.dhcp_sniffing_chk = wx.MenuItem(self.listen_menu, wx.ID_ANY, u'Listen for DHCP requests', wx.EmptyString, wx.ITEM_CHECK)
        self.listen_menu.AppendItem(self.dhcp_sniffing_chk)
        self.dhcp_sniffing_chk.Check(True)
        self.amx_only_filter_chk = wx.MenuItem(self.listen_menu, wx.ID_ANY, u'Only add AMX devices', wx.EmptyString, wx.ITEM_CHECK)
        self.listen_menu.AppendItem(self.amx_only_filter_chk)
        self.m_menubar1.Append(self.listen_menu, u'Listen')
        self.delete_menu = wx.Menu()
        self.delete_item_menu = wx.MenuItem(self.delete_menu, wx.ID_ANY, u'Delete Item', wx.EmptyString, wx.ITEM_NORMAL)
        self.delete_menu.AppendItem(self.delete_item_menu)
        self.delete_all_menu = wx.MenuItem(self.delete_menu, wx.ID_ANY, u'Delete All Items', wx.EmptyString, wx.ITEM_NORMAL)
        self.delete_menu.AppendItem(self.delete_all_menu)
        self.m_menubar1.Append(self.delete_menu, u'Delete')
        self.help_menu = wx.Menu()
        self.about_menu = wx.MenuItem(self.help_menu, wx.ID_ANY, u'About', wx.EmptyString, wx.ITEM_NORMAL)
        self.help_menu.AppendItem(self.about_menu)
        self.m_menubar1.Append(self.help_menu, u'Help')
        self.SetMenuBar(self.m_menubar1)
        self.status_bar = self.CreateStatusBar(2, wx.ST_SIZEGRIP, wx.ID_ANY)
        self.rc_menu = wx.Menu()
        self.update_rc_menu = wx.MenuItem(self.rc_menu, wx.ID_ANY, u'Update device information', wx.EmptyString, wx.ITEM_NORMAL)
        self.rc_menu.AppendItem(self.update_rc_menu)
        self.custom_script_rc_menu = wx.MenuItem(self.rc_menu, wx.ID_ANY, u'Custom Websocket Script', wx.EmptyString, wx.ITEM_NORMAL)
        self.rc_menu.AppendItem(self.custom_script_rc_menu)
        self.ping_rc_menu = wx.MenuItem(self.rc_menu, wx.ID_ANY, u'Ping Device', wx.EmptyString, wx.ITEM_NORMAL)
        self.rc_menu.AppendItem(self.ping_rc_menu)
        self.delete_rc_menu = wx.MenuItem(self.rc_menu, wx.ID_ANY, u'Delete', wx.EmptyString, wx.ITEM_NORMAL)
        self.rc_menu.AppendItem(self.delete_rc_menu)
        self.telnet_rc_menu = wx.MenuItem(self.rc_menu, wx.ID_ANY, u'Telnet to Device', wx.EmptyString, wx.ITEM_NORMAL)
        self.rc_menu.AppendItem(self.telnet_rc_menu)
        self.reboot_rc_menu = wx.MenuItem(self.rc_menu, wx.ID_ANY, u'Reboot Device', wx.EmptyString, wx.ITEM_NORMAL)
        self.rc_menu.AppendItem(self.reboot_rc_menu)
        self.browser_rc_menu = wx.MenuItem(self.rc_menu, wx.ID_ANY, u'Open device in Web Browser', wx.EmptyString, wx.ITEM_NORMAL)
        self.rc_menu.AppendItem(self.browser_rc_menu)
        self.Bind(wx.EVT_RIGHT_DOWN, self.MainFrameOnContextMenu)
        self.Centre(wx.BOTH)
        self.Bind(wx.EVT_CLOSE, self.on_close)
        self.Bind(wx.EVT_MENU, self.import_csv_file, id=self.import_csv_menu.GetId())
        self.Bind(wx.EVT_MENU, self.import_ip_list, id=self.import_ip_menu.GetId())
        self.Bind(wx.EVT_MENU, self.export_to_csv, id=self.export_csv_menu.GetId())
        self.Bind(wx.EVT_MENU, self.on_quit, id=self.quit_menu.GetId())
        self.Bind(wx.EVT_MENU, self.on_select_all, id=self.all_menu.GetId())
        self.Bind(wx.EVT_MENU, self.on_select_none, id=self.none_menu.GetId())
        self.Bind(wx.EVT_MENU, self.configure_prefs, id=self.preferences_menu.GetId())
        self.Bind(wx.EVT_MENU, self.update_device_information, id=self.update_device_menu.GetId())
        self.Bind(wx.EVT_MENU, self.telnet_to, id=self.telnet_menu.GetId())
        self.Bind(wx.EVT_MENU, self.ssh_to, id=self.ssh_menu.GetId())
        self.Bind(wx.EVT_MENU, self.reset_factory, id=self.reset_menu.GetId())
        self.Bind(wx.EVT_MENU, self.reboot, id=self.reboot_menu.GetId())
        self.Bind(wx.EVT_MENU, self.multi_ping, id=self.ping_menu.GetId())
        self.Bind(wx.EVT_MENU, self.add_line, id=self.add_menu.GetId())
        self.Bind(wx.EVT_MENU, self.generate_list, id=self.generate_menu.GetId())
        self.Bind(wx.EVT_MENU, self.load_certificate, id=self.cert_menu.GetId())
        self.Bind(wx.EVT_MENU, self.on_dhcp_sniffing, id=self.dhcp_sniffing_chk.GetId())
        self.Bind(wx.EVT_MENU, self.on_amx_only_filter, id=self.amx_only_filter_chk.GetId())
        self.Bind(wx.EVT_MENU, self.delete_item, id=self.delete_item_menu.GetId())
        self.Bind(wx.EVT_MENU, self.delete_all_items, id=self.delete_all_menu.GetId())
        self.Bind(wx.EVT_MENU, self.on_about_box, id=self.about_menu.GetId())
        self.Bind(wx.EVT_MENU, self.update_device_information, id=self.update_rc_menu.GetId())
        self.Bind(wx.EVT_MENU, self.custom_websocket_script, id=self.custom_script_rc_menu.GetId())
        self.Bind(wx.EVT_MENU, self.multi_ping, id=self.ping_rc_menu.GetId())
        self.Bind(wx.EVT_MENU, self.delete_item, id=self.delete_rc_menu.GetId())
        self.Bind(wx.EVT_MENU, self.telnet_to, id=self.telnet_rc_menu.GetId())
        self.Bind(wx.EVT_MENU, self.reboot, id=self.reboot_rc_menu.GetId())
        self.Bind(wx.EVT_MENU, self.open_url, id=self.browser_rc_menu.GetId())

    def __del__(self):
        pass

    def on_close(self, event):
        event.Skip()

    def import_csv_file(self, event):
        event.Skip()

    def import_ip_list(self, event):
        event.Skip()

    def export_to_csv(self, event):
        event.Skip()

    def on_quit(self, event):
        event.Skip()

    def on_select_all(self, event):
        event.Skip()

    def on_select_none(self, event):
        event.Skip()

    def configure_prefs(self, event):
        event.Skip()

    def update_device_information(self, event):
        event.Skip()

    def telnet_to(self, event):
        event.Skip()

    def ssh_to(self, event):
        event.Skip()

    def reset_factory(self, event):
        event.Skip()

    def reboot(self, event):
        event.Skip()

    def multi_ping(self, event):
        event.Skip()

    def add_line(self, event):
        event.Skip()

    def generate_list(self, event):
        event.Skip()

    def load_certificate(self, event):
        event.Skip()

    def on_dhcp_sniffing(self, event):
        event.Skip()

    def on_amx_only_filter(self, event):
        event.Skip()

    def delete_item(self, event):
        event.Skip()

    def delete_all_items(self, event):
        event.Skip()

    def on_about_box(self, event):
        event.Skip()

    def custom_websocket_script(self, event):
        event.Skip()

    def open_url(self, event):
        event.Skip()

    def MainFrameOnContextMenu(self, event):
        self.PopupMenu(self.rc_menu, event.GetPosition())


class PingDetail(wx.Dialog):

    def __init__(self, parent):
        wx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u'Details view', pos=wx.DefaultPosition, size=wx.Size(390, 550), style=wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER)
        self.SetSizeHintsSz(wx.DefaultSize, wx.DefaultSize)
        bSizer21 = wx.BoxSizer(wx.VERTICAL)
        self.olv_panel = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        bSizer24 = wx.BoxSizer(wx.VERTICAL)
        self.olv_sizer = wx.BoxSizer(wx.VERTICAL)
        bSizer24.Add(self.olv_sizer, 1, wx.EXPAND, 5)
        bSizer23 = wx.BoxSizer(wx.HORIZONTAL)
        bSizer26 = wx.BoxSizer(wx.HORIZONTAL)
        self.auto_update_chk = wx.CheckBox(self.olv_panel, wx.ID_ANY, u'Auto update', wx.DefaultPosition, wx.DefaultSize, 0)
        bSizer26.Add(self.auto_update_chk, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)
        bSizer23.Add(bSizer26, 1, wx.ALIGN_CENTER_VERTICAL, 5)
        bSizer27 = wx.BoxSizer(wx.VERTICAL)
        self.m_button4 = wx.Button(self.olv_panel, wx.ID_ANY, u'Refresh', wx.DefaultPosition, wx.DefaultSize, 0)
        bSizer27.Add(self.m_button4, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)
        bSizer23.Add(bSizer27, 1, 0, 5)
        bSizer24.Add(bSizer23, 0, wx.EXPAND, 5)
        self.olv_panel.SetSizer(bSizer24)
        self.olv_panel.Layout()
        bSizer24.Fit(self.olv_panel)
        bSizer21.Add(self.olv_panel, 1, wx.EXPAND, 5)
        self.SetSizer(bSizer21)
        self.Layout()
        self.Centre(wx.BOTH)
        self.auto_update_chk.Bind(wx.EVT_CHECKBOX, self.on_auto_update)
        self.m_button4.Bind(wx.EVT_BUTTON, self.on_refresh)

    def __del__(self):
        pass

    def on_auto_update(self, event):
        event.Skip()

    def on_refresh(self, event):
        event.Skip()


class Preferences(wx.Dialog):

    def __init__(self, parent):
        wx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u'Preferences', pos=wx.DefaultPosition, size=wx.Size(-1, -1), style=wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER)
        self.SetSizeHintsSz(wx.DefaultSize, wx.DefaultSize)
        bSizer10 = wx.BoxSizer(wx.VERTICAL)
        sbSizer14 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u'Security'), wx.VERTICAL)
        bSizer621 = wx.BoxSizer(wx.VERTICAL)
        bSizer59 = wx.BoxSizer(wx.VERTICAL)
        self.use_password_chk = wx.CheckBox(sbSizer14.GetStaticBox(), wx.ID_ANY, u'Use Password', wx.DefaultPosition, wx.DefaultSize, 0)
        self.use_password_chk.Hide()
        bSizer59.Add(self.use_password_chk, 0, wx.ALL, 5)
        bSizer621.Add(bSizer59, 1, wx.EXPAND, 5)
        bSizer63 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText29 = wx.StaticText(sbSizer14.GetStaticBox(), wx.ID_ANY, u'Username', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText29.Wrap(-1)
        bSizer63.Add(self.m_staticText29, 0, wx.ALL, 5)
        self.username_txt = wx.TextCtrl(sbSizer14.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
        bSizer63.Add(self.username_txt, 1, wx.ALL, 5)
        bSizer621.Add(bSizer63, 0, wx.EXPAND, 5)
        bSizer64 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText30 = wx.StaticText(sbSizer14.GetStaticBox(), wx.ID_ANY, u'Password', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText30.Wrap(-1)
        bSizer64.Add(self.m_staticText30, 0, wx.ALL, 5)
        self.password_txt = wx.TextCtrl(sbSizer14.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
        bSizer64.Add(self.password_txt, 1, wx.ALL, 5)
        bSizer621.Add(bSizer64, 0, wx.EXPAND, 5)
        sbSizer14.Add(bSizer621, 1, wx.EXPAND, 5)
        bSizer10.Add(sbSizer14, 0, wx.EXPAND | wx.ALL, 5)
        sbSizer2 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u'Notifications'), wx.VERTICAL)
        self.sounds_chk = wx.CheckBox(sbSizer2.GetStaticBox(), wx.ID_ANY, u'Play Sounds', wx.DefaultPosition, wx.DefaultSize, 0)
        sbSizer2.Add(self.sounds_chk, 0, wx.ALL, 5)
        self.script_messages_chk = wx.CheckBox(sbSizer2.GetStaticBox(), wx.ID_ANY, u'Show Script Messages', wx.DefaultPosition, wx.DefaultSize, 0)
        sbSizer2.Add(self.script_messages_chk, 0, wx.ALL, 5)
        bSizer10.Add(sbSizer2, 0, wx.EXPAND | wx.ALL, 5)
        sbSizer3 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u'Columns To Display'), wx.VERTICAL)
        gSizer1 = wx.GridSizer(0, 2, 0, 0)
        self.model_chk = wx.CheckBox(sbSizer3.GetStaticBox(), wx.ID_ANY, u'Model', wx.DefaultPosition, wx.DefaultSize, 0)
        gSizer1.Add(self.model_chk, 0, wx.ALL, 5)
        self.mac_chk = wx.CheckBox(sbSizer3.GetStaticBox(), wx.ID_ANY, u'MAC', wx.DefaultPosition, wx.DefaultSize, 0)
        gSizer1.Add(self.mac_chk, 0, wx.ALL, 5)
        self.hostname_chk = wx.CheckBox(sbSizer3.GetStaticBox(), wx.ID_ANY, u'Hostname', wx.DefaultPosition, wx.DefaultSize, 0)
        gSizer1.Add(self.hostname_chk, 0, wx.ALL, 5)
        self.serial_chk = wx.CheckBox(sbSizer3.GetStaticBox(), wx.ID_ANY, u'Serial Number', wx.DefaultPosition, wx.DefaultSize, 0)
        gSizer1.Add(self.serial_chk, 0, wx.ALL, 5)
        self.firmware_chk = wx.CheckBox(sbSizer3.GetStaticBox(), wx.ID_ANY, u'Firmware', wx.DefaultPosition, wx.DefaultSize, 0)
        gSizer1.Add(self.firmware_chk, 0, wx.ALL, 5)
        self.static_chk = wx.CheckBox(sbSizer3.GetStaticBox(), wx.ID_ANY, u'Static', wx.DefaultPosition, wx.DefaultSize, 0)
        gSizer1.Add(self.static_chk, 0, wx.ALL, 5)
        sbSizer3.Add(gSizer1, 0, wx.EXPAND, 5)
        bSizer10.Add(sbSizer3, 0, wx.EXPAND | wx.ALL, 5)
        m_sdbSizer3 = wx.StdDialogButtonSizer()
        self.m_sdbSizer3OK = wx.Button(self, wx.ID_OK)
        m_sdbSizer3.AddButton(self.m_sdbSizer3OK)
        self.m_sdbSizer3Cancel = wx.Button(self, wx.ID_CANCEL)
        m_sdbSizer3.AddButton(self.m_sdbSizer3Cancel)
        m_sdbSizer3.Realize()
        bSizer10.Add(m_sdbSizer3, 0, wx.ALL | wx.EXPAND, 5)
        self.SetSizer(bSizer10)
        self.Layout()
        bSizer10.Fit(self)
        self.Centre(wx.BOTH)
        self.use_password_chk.Bind(wx.EVT_CHECKBOX, self.on_password)
        self.m_sdbSizer3Cancel.Bind(wx.EVT_BUTTON, self.on_cancel)
        self.m_sdbSizer3OK.Bind(wx.EVT_BUTTON, self.on_ok)

    def __del__(self):
        pass

    def on_password(self, event):
        event.Skip()

    def on_cancel(self, event):
        event.Skip()

    def on_ok(self, event):
        event.Skip()


class MultiPing(wx.Dialog):

    def __init__(self, parent):
        wx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=wx.EmptyString, pos=wx.DefaultPosition, size=wx.Size(730, 300), style=wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER)
        self.SetSizeHintsSz(wx.DefaultSize, wx.DefaultSize)
        bSizer4 = wx.BoxSizer(wx.VERTICAL)
        bSizer44 = wx.BoxSizer(wx.VERTICAL)
        self.olv_panel = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        self.olv_sizer = wx.BoxSizer(wx.VERTICAL)
        self.olv_panel.SetSizer(self.olv_sizer)
        self.olv_panel.Layout()
        self.olv_sizer.Fit(self.olv_panel)
        bSizer44.Add(self.olv_panel, 1, wx.EXPAND | wx.ALL, 5)
        bSizer4.Add(bSizer44, 1, wx.EXPAND, 5)
        bSizer45 = wx.BoxSizer(wx.VERTICAL)
        sbSizer8 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u'Logging'), wx.VERTICAL)
        self.log_enable_chk = wx.CheckBox(sbSizer8.GetStaticBox(), wx.ID_ANY, u'Log to file', wx.DefaultPosition, wx.DefaultSize, 0)
        sbSizer8.Add(self.log_enable_chk, 0, wx.ALL, 5)
        self.log_file_txt = wx.StaticText(sbSizer8.GetStaticBox(), wx.ID_ANY, u'logfile', wx.DefaultPosition, wx.DefaultSize, 0)
        self.log_file_txt.Wrap(-1)
        sbSizer8.Add(self.log_file_txt, 0, wx.ALL, 5)
        bSizer45.Add(sbSizer8, 0, wx.EXPAND, 5)
        bSizer4.Add(bSizer45, 0, wx.EXPAND, 5)
        self.SetSizer(bSizer4)
        self.Layout()
        self.rc_menu = wx.Menu()
        self.m_menuItem37 = wx.MenuItem(self.rc_menu, wx.ID_ANY, u'Show Details', wx.EmptyString, wx.ITEM_NORMAL)
        self.rc_menu.AppendItem(self.m_menuItem37)
        self.Bind(wx.EVT_RIGHT_DOWN, self.MultiPingOnContextMenu)
        self.Centre(wx.BOTH)
        self.log_enable_chk.Bind(wx.EVT_CHECKBOX, self.on_log_enable)
        self.Bind(wx.EVT_MENU, self.on_show_details, id=self.m_menuItem37.GetId())

    def __del__(self):
        pass

    def on_log_enable(self, event):
        event.Skip()

    def on_show_details(self, event):
        event.Skip()

    def MultiPingOnContextMenu(self, event):
        self.PopupMenu(self.rc_menu, event.GetPosition())


class GenerateIP(wx.Dialog):

    def __init__(self, parent):
        wx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u'Generate IP list', pos=wx.DefaultPosition, size=wx.DefaultSize, style=wx.DEFAULT_DIALOG_STYLE)
        self.SetSizeHintsSz(wx.DefaultSize, wx.DefaultSize)
        bSizer27 = wx.BoxSizer(wx.VERTICAL)
        self.m_panel4 = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        bSizer28 = wx.BoxSizer(wx.VERTICAL)
        bSizer29 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText9 = wx.StaticText(self.m_panel4, wx.ID_ANY, u'Starting IP', wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE)
        self.m_staticText9.Wrap(-1)
        bSizer29.Add(self.m_staticText9, 1, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)
        self.start_txt = wx.TextCtrl(self.m_panel4, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
        self.start_txt.SetMinSize(wx.Size(200, -1))
        bSizer29.Add(self.start_txt, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)
        bSizer28.Add(bSizer29, 1, wx.EXPAND, 5)
        bSizer30 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText10 = wx.StaticText(self.m_panel4, wx.ID_ANY, u'Finishing IP', wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE)
        self.m_staticText10.Wrap(-1)
        bSizer30.Add(self.m_staticText10, 1, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)
        self.finish_txt = wx.TextCtrl(self.m_panel4, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
        self.finish_txt.SetMinSize(wx.Size(200, -1))
        bSizer30.Add(self.finish_txt, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)
        bSizer28.Add(bSizer30, 1, wx.EXPAND, 5)
        bSizer31 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_button5 = wx.Button(self.m_panel4, wx.ID_ANY, u'Replace List', wx.DefaultPosition, wx.DefaultSize, 0)
        bSizer31.Add(self.m_button5, 0, wx.ALL, 5)
        self.m_button6 = wx.Button(self.m_panel4, wx.ID_ANY, u'Add to List', wx.DefaultPosition, wx.DefaultSize, 0)
        bSizer31.Add(self.m_button6, 0, wx.ALL, 5)
        self.m_button7 = wx.Button(self.m_panel4, wx.ID_ANY, u'Save as File', wx.DefaultPosition, wx.DefaultSize, 0)
        bSizer31.Add(self.m_button7, 0, wx.ALL, 5)
        bSizer28.Add(bSizer31, 1, wx.EXPAND | wx.ALIGN_CENTER_HORIZONTAL, 5)
        self.m_panel4.SetSizer(bSizer28)
        self.m_panel4.Layout()
        bSizer28.Fit(self.m_panel4)
        bSizer27.Add(self.m_panel4, 1, wx.EXPAND | wx.ALL, 5)
        self.SetSizer(bSizer27)
        self.Layout()
        bSizer27.Fit(self)
        self.Centre(wx.BOTH)
        self.m_button5.Bind(wx.EVT_BUTTON, self.on_action)
        self.m_button6.Bind(wx.EVT_BUTTON, self.on_action)
        self.m_button7.Bind(wx.EVT_BUTTON, self.on_save)

    def __del__(self):
        pass

    def on_action(self, event):
        event.Skip()

    def on_save(self, event):
        event.Skip()


class PasswordChange(wx.Dialog):

    def __init__(self, parent):
        wx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u'Change Adminstrator Password', pos=wx.DefaultPosition, size=wx.DefaultSize, style=wx.DEFAULT_DIALOG_STYLE)
        self.SetSizeHintsSz(wx.DefaultSize, wx.DefaultSize)
        bSizer31 = wx.BoxSizer(wx.VERTICAL)
        sbSizer10 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u'Old Password'), wx.VERTICAL)
        bSizer30 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText7 = wx.StaticText(sbSizer10.GetStaticBox(), wx.ID_ANY, u'Encrypted Password', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText7.Wrap(-1)
        bSizer30.Add(self.m_staticText7, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)
        self.old_encrypt_txt = wx.TextCtrl(sbSizer10.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_CHARWRAP | wx.TE_MULTILINE | wx.TE_RICH | wx.TE_WORDWRAP)
        self.old_encrypt_txt.SetMinSize(wx.Size(300, 80))
        bSizer30.Add(self.old_encrypt_txt, 1, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)
        sbSizer10.Add(bSizer30, 1, wx.EXPAND, 5)
        bSizer31.Add(sbSizer10, 1, wx.EXPAND | wx.ALL, 5)
        sbSizer11 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u'New Password'), wx.VERTICAL)
        bSizer301 = wx.BoxSizer(wx.HORIZONTAL)
        self.m_staticText71 = wx.StaticText(sbSizer11.GetStaticBox(), wx.ID_ANY, u'Encrypted Password', wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText71.Wrap(-1)
        bSizer301.Add(self.m_staticText71, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)
        self.new_encrypt_txt = wx.TextCtrl(sbSizer11.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_CHARWRAP | wx.TE_MULTILINE | wx.TE_RICH | wx.TE_WORDWRAP)
        self.new_encrypt_txt.SetMinSize(wx.Size(300, 80))
        bSizer301.Add(self.new_encrypt_txt, 1, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)
        sbSizer11.Add(bSizer301, 1, wx.EXPAND, 5)
        bSizer31.Add(sbSizer11, 1, wx.EXPAND | wx.ALL, 5)
        m_sdbSizer2 = wx.StdDialogButtonSizer()
        self.m_sdbSizer2OK = wx.Button(self, wx.ID_OK)
        m_sdbSizer2.AddButton(self.m_sdbSizer2OK)
        self.m_sdbSizer2Cancel = wx.Button(self, wx.ID_CANCEL)
        m_sdbSizer2.AddButton(self.m_sdbSizer2Cancel)
        m_sdbSizer2.Realize()
        bSizer31.Add(m_sdbSizer2, 0, wx.EXPAND | wx.ALL, 5)
        self.SetSizer(bSizer31)
        self.Layout()
        bSizer31.Fit(self)
        self.Centre(wx.BOTH)

    def __del__(self):
        pass


class Log(wx.Dialog):

    def __init__(self, parent):
        wx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u'Log', pos=wx.DefaultPosition, size=wx.Size(615, 329), style=wx.DEFAULT_DIALOG_STYLE)
        self.SetSizeHintsSz(wx.DefaultSize, wx.DefaultSize)
        bSizer60 = wx.BoxSizer(wx.VERTICAL)
        self.m_panel9 = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        bSizer61 = wx.BoxSizer(wx.VERTICAL)
        self.log_txt = wx.TextCtrl(self.m_panel9, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_MULTILINE | wx.TE_READONLY)
        bSizer61.Add(self.log_txt, 1, wx.ALL | wx.EXPAND, 5)
        self.m_panel9.SetSizer(bSizer61)
        self.m_panel9.Layout()
        bSizer61.Fit(self.m_panel9)
        bSizer60.Add(self.m_panel9, 1, wx.EXPAND | wx.ALL, 5)
        self.SetSizer(bSizer60)
        self.Layout()
        self.Centre(wx.BOTH)

    def __del__(self):
        pass