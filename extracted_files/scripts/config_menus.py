# Embedded file name: scripts\config_menus.py
"""Menus for Magic NX Firmware Updater"""
import wx
import csv
from scripts import nss_gui
from netaddr import IPRange

class PreferencesConfig(nss_gui.Preferences):
    """Sets the preferences """

    def __init__(self, parent):
        nss_gui.Preferences.__init__(self, parent)
        self.parent = parent
        self.set_values()

    def set_values(self):
        """Set the field values"""
        self.sounds_chk.SetValue(int(self.parent.play_sounds))
        self.username_txt.SetValue(self.parent.username)
        self.password_txt.SetValue(self.parent.password)
        self.script_messages_chk.SetValue(self.parent.show_script_messages)
        if self.parent.columns_config != ['']:
            for item in self.parent.columns_config:
                getattr(self, item.lower() + '_chk').SetValue(True)

    def on_ok(self, _):
        """When user clicks ok"""
        self.parent.columns_config = []
        for item in self.parent.columns_default.split(', '):
            if getattr(self, item.lower() + '_chk').GetValue():
                self.parent.columns_config.append(item)

        self.parent.password = str(self.password_txt.GetValue())
        self.parent.username = str(self.username_txt.GetValue())
        self.parent.play_sounds = self.sounds_chk.GetValue()
        self.parent.show_script_messages = self.script_messages_chk.GetValue()
        self.parent.write_config_file()
        self.parent.select_columns()
        self.parent.resize_frame()
        self.Destroy()

    def on_cancel(self, _):
        """When user clicks cancel"""
        self.Destroy()


class IpListGen(nss_gui.GenerateIP):
    """Generates a list of IP's"""

    def __init__(self, parent):
        nss_gui.GenerateIP.__init__(self, parent)
        self.parent = parent
        self.start_txt.SetLabel('192.168.1.1')
        self.finish_txt.SetLabel('192.168.1.100')
        self.data = []
        self.SetTitle('Generate IP list')

    def on_action(self, event):
        """Testing event"""
        if not self.gen_list():
            return
        if self.check_size():
            if event.GetEventObject().GetLabel() == 'Add to List':
                self.on_add()
            elif event.GetEventObject().GetLabel() == 'Replace List':
                self.on_replace()
        else:
            return

    def on_replace(self):
        """Replaces list with generated list"""
        if not self.gen_list():
            return
        self.parent.main_list.DeleteAllItems()
        for item in self.data:
            self.parent.create_add_unit(ip_ad=str(item))

        self.parent.dump_pickle()
        self.Destroy()

    def on_add(self):
        """Adds to the bottom of the list"""
        if not self.gen_list():
            return
        for item in self.data:
            self.parent.create_add_unit(ip_ad=str(item))

        self.parent.dump_pickle()
        self.Destroy()

    def on_save(self, _):
        """Saves the ip list to a file"""
        if not self.gen_list():
            return
        if self.check_size():
            save_file_dialog = wx.FileDialog(self, message='Save IP list', defaultDir=self.parent.path, defaultFile='generatediplist.csv', wildcard='CSV files (*.csv)|*.csv', style=wx.SAVE)
            if save_file_dialog.ShowModal() == wx.ID_OK:
                path = save_file_dialog.GetPath()
                with open(path, 'wb') as ip_list_file:
                    writer_csv = csv.writer(ip_list_file)
                    for item in self.data:
                        writer_csv.writerow([item])

                    self.Destroy()
            else:
                self.Destroy()
        else:
            return

    def gen_list(self):
        """Generates the IP list"""
        self.data = []
        try:
            ip_range = IPRange(self.start_txt.GetValue(), self.finish_txt.GetValue())
        except ValueError:
            dlg = wx.MessageDialog(parent=self, message='The IP address entered is invalid', caption='Invalid IP', style=wx.OK)
            dlg.ShowModal()
            return False

        for address in list(ip_range):
            self.data.append(str(address))

        return True

    def check_size(self):
        """Warning abou the size of the range"""
        if len(self.data) > 500:
            dlg = wx.MessageDialog(parent=self, message='Are you sure? \n\nThis will create ' + str(len(self.data)) + " IP's. Creating more than 1000 IP's could" + ' slow or even crash the program', caption='Creating IP list', style=wx.OK | wx.CANCEL)
            if dlg.ShowModal() == wx.ID_OK:
                return True
            else:
                return False
        else:
            return True


class AdminPasswordChange(nss_gui.PasswordChange):
    """Gets Admin passwords for changing"""

    def __init__(self, parent):
        nss_gui.PasswordChange.__init__(self, parent)
        self.parent = parent
        self.old_encrypt_txt.SetLabel('192.168.1.1')
        self.new_encrypt_txt.SetLabel('192.168.1.100')
        self.SetTitle('Enter Admin Password')
