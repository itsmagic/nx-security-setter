"""Configurator is a program that integrates device discovery and telnet
commands to ease configuration and management of AMX DXLink devices.

The MIT License (MIT)

Copyright (c) 2019 Jim Maciejewski

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE."""

from os import path, listdir, mkdir, remove
import sys
import datetime
import pickle
import wx
import csv
from ObjectListView import FastObjectListView as ObjectListView, ColumnDefn, EVT_CELL_EDIT_FINISHING
import queue
import webbrowser
from requests import get
import random
from pydispatch import dispatcher

from scripts import (auto_update, config_menus, datastore, dhcp_sniffer, dhcpjobs_class, nss_gui,
                     multi_ping, multi_ping_model, telnet_class, telnetto_class, websocket_class)


class NX_Security_Setter_Frame(nss_gui.NX_Security_Setter_Frame):
    def __init__(self, parent):
        nss_gui.NX_Security_Setter_Frame.__init__(self, parent)

        icon_bundle = wx.IconBundle()
        icon_bundle.AddIcon(path.join("icon", "nss.ico"), wx.BITMAP_TYPE_ANY)
        self.SetIcons(icon_bundle)
        self.name = "NX Security Setter"
        self.version = "v1.0.1"
        self.storage_path = path.expanduser(path.join('~', 'Documents', self.name))
        if not path.exists(self.storage_path):
            mkdir(self.storage_path)
        self.storage_file = "_".join(self.name.split()) + ".pkl"
        self.SetTitle(self.name + " " + self.version)

        pick = self.load_config()
        self.preferences = pick['preferences']
        self.preferences.set_prefs(self.storage_path)
        if self.preferences.telnet_client is None:
            self.telnet_missing_dia()

        self.main_list = ObjectListView(self.olv_panel, wx.ID_ANY,
                                        style=wx.LC_REPORT | wx.SUNKEN_BORDER)
        self.main_list.cellEditMode = ObjectListView.CELLEDIT_DOUBLECLICK
        self.main_list.Bind(EVT_CELL_EDIT_FINISHING, self.save_main_list)
        self.main_list.Bind(wx.EVT_LIST_ITEM_RIGHT_CLICK,
                            self.olv_panelOnContextMenu)
        self.main_list.Bind(wx.EVT_KEY_DOWN, self.on_key_down)

        self.columns = [
            ColumnDefn("Time", "left", 110, "arrival_time", stringConverter="%I:%M:%S%p"),
            ColumnDefn("Model", "left", 160, "model"),
            ColumnDefn("MAC", "left", 125, "mac_address"),
            ColumnDefn("IP", "left", 120, "ip_address"),
            ColumnDefn("Hostname", "left", 150, "hostname"),
            ColumnDefn("Serial", "left", 150, "serial"),
            ColumnDefn("Firmware", "left", 100, "firmware"),
            ColumnDefn("Static", "center", 30, "ip_type"),
            ColumnDefn("Status", "center", 120, "status")]

        self.set_selected_columns()
        self.main_list.SetEmptyListMsg("Select Tools-->Add Item to add an individual item")
        self.olv_sizer.Add(self.main_list, 1, wx.ALL | wx.EXPAND, 0)
        self.olv_sizer.Layout()

        for item in pick['main_list']:
            item.status = ""
            self.main_list.AddObject(item)

        self.errorlist = []
        self.completionlist = []

        self.amx_only_filter_chk.Check(self.preferences.amx_only_filter)
        self.dhcp_sniffing_chk.Check(self.preferences.dhcp_listen)

        # Create DHCP listening thread
        self.dhcp_listener = dhcp_sniffer.DHCPListener()
        self.dhcp_listener.setDaemon(True)
        self.dhcp_listener.start()

        # create a telenetto thread pool and assign them to a queue
        self.telnet_to_queue = queue.Queue()
        for _ in range(10):
            self.telnet_to_thread = telnetto_class.TelnetToThread(
                self, self.telnet_to_queue)
            self.telnet_to_thread.setDaemon(True)
            self.telnet_to_thread.start()

        # create a telnetjob thread pool and assign them to a queue
        self.telnet_job_queue = queue.Queue()
        for _ in range(int(self.preferences.number_of_threads)):
            self.telnet_job_thread = telnet_class.Telnetjobs(
                self, self.telnet_job_queue)
            self.telnet_job_thread.setDaemon(True)
            self.telnet_job_thread.start()

        self.websocket_job_queue = queue.Queue()
        for _ in range(int(self.preferences.number_of_threads)):
            self.websocket_job_thread = websocket_class.Websocketjobs(
                self, self.websocket_job_queue)
            self.websocket_job_thread.setDaemon(True)
            self.websocket_job_thread.start()

        self.dhcp_job_queue = queue.Queue()
        self.dhcp_job_thread = dhcpjobs_class.DHCPjobs(self, self.dhcp_job_queue)
        self.dhcp_job_thread.setDaemon(True)
        self.dhcp_job_thread.start()

        self.ping_window = multi_ping.MultiPing(self)
        self.ping_window.Hide()
        self.ping_model = multi_ping_model.MultiPing_Model(self.storage_path)

        dispatcher.connect(self.incoming_dhcp,
                           signal="Incoming DHCP",
                           sender=dispatcher.Any)
        dispatcher.connect(self.set_status,
                           signal="Status Update",
                           sender=dispatcher.Any)
        dispatcher.connect(self.collect_completions,
                           signal="Collect Completions",
                           sender=dispatcher.Any)
        dispatcher.connect(self.collect_errors,
                           signal="Collect Errors",
                           sender=dispatcher.Any)
        self.dhcp_listener.dhcp_sniffing_enabled = self.preferences.dhcp_listen

        self.check_for_updates = True
        dispatcher.connect(self.update_required,
                           signal="Software Update",
                           sender=dispatcher.Any)

        if self.check_for_updates:
            update_thread = auto_update.AutoUpdate(server_url="https://magicsoftware.ornear.com", program_name=self.name, program_version=self.version)
            update_thread.setDaemon(True)
            update_thread.start()

    def resource_path(self, relative):
        return path.join(getattr(sys, '_MEIPASS', path.abspath(".")),
                         relative)

    def update_required(self, sender, message, url):
        """Show the update page"""
        dlg = wx.MessageDialog(parent=self,
                               message='An update is available. \rWould you like to go to the download page?',
                               caption='Update available',
                               style=wx.OK | wx.CANCEL)

        if dlg.ShowModal() == wx.ID_OK:
            webbrowser.open(url)

    def on_key_down(self, event):
        """Grab Delete key presses"""
        key = event.GetKeyCode()
        if key == wx.WXK_DELETE:
            dlg = wx.MessageDialog(
                parent=self,
                message='Are you sure? \n\nThis will delete all selected items in the list',
                caption='Delete All Selected Items',
                style=wx.OK | wx.CANCEL)

            if dlg.ShowModal() == wx.ID_OK:
                self.on_delete_item(None)
                self.save_main_list()
            else:
                return
        event.Skip()

    def play_sound(self):
        """Plays a barking sound"""
        sounds_list = []
        if self.preferences.play_sounds:
            if self.preferences.randomize_sounds:
                for file in listdir('sounds'):
                    if file.endswith('.wav'):
                        sounds_list.append(file)
                filename = random.choice(sounds_list)
            else:
                filename = 'woof.wav'
            sound = wx.adv.Sound(path.join('sounds', filename))
            if sound.IsOk():
                sound.Play(wx.adv.SOUND_ASYNC)
            else:
                wx.MessageBox("Invalid sound file", "Error")

    def set_selected_columns(self):
        """Sets the preferred columns"""
        todisplay = []
        for item in self.columns:
            if item.title in self.preferences.cols_selected:
                todisplay.append(item)
        self.main_list.SetColumns(todisplay)
        self.SetSize((self.main_list.GetBestSize()[0] + 40, 600))

    def set_status(self, sender):
        """sets the status of an object from a tuple of (obj, status)"""
        sender[0].status = sender[1]
        self.main_list.RefreshObject(sender[0])

    def communication_started(self, sender):
        """Updates status when communication is started"""
        sender.status = "Connecting"
        self.main_list.RefreshObject(sender)

    def collect_completions(self, sender):
        """Creates a list of completed connections"""
        self.completionlist.append(sender)
        sender.status = "Success"
        self.main_list.RefreshObject(sender)

    def collect_errors(self, sender):
        """Creates a list of incomplete connections"""
        self.errorlist.append(sender)
        sender[0].status = "Failed: " + sender[1]
        self.main_list.RefreshObject(sender[0])

    def port_errors(self):
        """Shows when the listening port is in use"""
        dlg = wx.MessageDialog(
            self,
            message='Unable to use port 67\n No DHCP requests will be added.',
            caption='Port in use',
            style=wx.ICON_INFORMATION)
        dlg.ShowModal()
        self.dhcp_sniffing_chk.Enable(False)
        self.amx_only_filter_chk.Enable(False)

    def on_dhcp_sniffing(self, _):
        """Turns sniffing on and off"""
        self.preferences.dhcp_listen = not self.preferences.dhcp_listen
        self.dhcp_sniffing_chk.Check(self.preferences.dhcp_listen)
        # self.dhcp_listener.dhcp_sniffing_enabled = self.preferences.dhcp_listen
        self.save_main_list()

    def on_amx_only_filter(self, _):
        """Turns amx filtering on and off"""
        self.preferences.amx_only_filter = not self.preferences.amx_only_filter
        self.amx_only_filter_chk.Check(self.preferences.amx_only_filter)
        self.save_main_list()

    def check_for_none_selected(self):
        """Checks if nothing is selected"""
        if len(self.main_list.GetSelectedObjects()) == 0:
            dlg = wx.MessageDialog(
                parent=self, message='Nothing selected...\nPlease click on the device you want to select',
                caption='Nothing Selected',
                style=wx.OK)
            dlg.ShowModal()
            dlg.Destroy()
            return True
        for obj in self.main_list.GetSelectedObjects():
            self.set_status((obj, ''))

    def on_select_all(self, _):
        """Select all items in the list"""
        self.main_list.SelectAll()

    def on_select_none(self, _):
        """Select none of the items in the list"""
        self.main_list.DeselectAll()

    def telnet_to(self, _):
        """Telnet to the selected device(s)"""
        if self.check_for_none_selected():
            return
        if len(self.main_list.GetSelectedObjects()) > 10:
            dlg = wx.MessageDialog(
                parent=self,
                message='I can only telnet to 10 devices at a time \nPlease select less than ten devices at once',
                caption='How many telnets?',
                style=wx.OK)
            dlg.ShowModal()
            dlg.Destroy()
            return

        if self.preferences.telnet_client is not None:

            for obj in self.main_list.GetSelectedObjects():
                self.telnet_to_queue.put([obj, 'telnet'])
                self.set_status((obj, "Queued"))
        else:
            message = f"Putty.exe was not found during startup.\r\rIf you want to telnet to a device you will\r need to download putty.exe to\r {self.storage_path}\r and restart {self.name}"
            dlg = wx.MessageDialog(parent=self,
                                   message=message,
                                   caption='Unable to find putty.exe',
                                   style=wx.OK)
            dlg.ShowModal()
            dlg.Destroy()

    def ssh_to(self, _):
        """Telnet to the selected device(s)"""
        if self.check_for_none_selected():
            return
        if len(self.main_list.GetSelectedObjects()) > 10:
            dlg = wx.MessageDialog(
                parent=self, message='I can only ssh to 10 devices at a time \nPlease select less than ten devices at once',
                caption='How many ssh?',
                style=wx.OK)
            dlg.ShowModal()
            dlg.Destroy()
            return
        if path.exists(path.join(self.storage_path, self.preferences.telnet_client)):

            for obj in self.main_list.GetSelectedObjects():
                self.telnet_to_queue.put([obj, 'ssh'])
                self.set_status((obj, "Queued"))
        else:
            dlg = wx.MessageDialog(
                parent=self,
                message=f'Could not find telnet client \nPlease put {self.preferences.telnet_client} in \n{self.storage_path}',
                caption='No %s' % self.preferences.telnet_client,
                style=wx.OK)
            dlg.ShowModal()
            dlg.Destroy()
        return

    def multi_ping(self, event):
        """Ping and track results of many devices --
        Hide and show window if neccesarry"""
        if self.check_for_none_selected():
            return
        self.ping_window.Show()
        self.ping_model.add(self.main_list.GetSelectedObjects())

    def multi_ping_remove(self, obj):
        """Removes an item from multiping"""
        self.ping_model.delete(obj)

    def multi_ping_logging(self):
        self.ping_model.toggle_logging()

    def multi_ping_shutdown(self):
        """Shuts down multi-ping"""
        self.ping_model.shutdown()

    def custom_websocket_script(self, _):
        """Do some websocket stuff in a script"""
        if self.check_for_none_selected():
            return
        open_file_dialog = wx.FileDialog(self, message="Load Custom Script",
                                         defaultDir=self.storage_path,
                                         defaultFile="",
                                         wildcard="Text files (*.txt)|*.txt",
                                         style=wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)
        if open_file_dialog.ShowModal() == wx.ID_OK:
            websocket_script_path = open_file_dialog.GetPath()
            open_file_dialog.Destroy()
        else:
            open_file_dialog.Destroy()
            return
        for obj in self.main_list.GetSelectedObjects():
            self.websocket_job_queue.put(['custom_websocket_script', obj, websocket_script_path])
            self.set_status((obj, "Queued"))

    def load_certificate(self, _):
        """Load ldap ceritifcate on server"""
        if self.check_for_none_selected():
            return
        open_file_dialog = wx.FileDialog(self, message="Load LDAP certificate",
                                         defaultDir=self.path,
                                         defaultFile="",
                                         wildcard="PEM files (*.pem)|*.pem",
                                         style=wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)
        if open_file_dialog.ShowModal() == wx.ID_OK:
            open_file_dialog.Destroy()
        else:
            open_file_dialog.Destroy()
            return
        for obj in self.main_list.GetSelectedObjects():
            self.websocket_job_queue.put(['send_cert', obj, open_file_dialog.GetPath()])
            self.set_status((obj, "Queued"))

    def reboot(self, _):
        """Reboots device"""
        if self.check_for_none_selected():
            return
        for obj in self.main_list.GetSelectedObjects():
            self.telnet_job_queue.put(['reboot', obj,
                                       self.preferences.telnet_timeout])
            self.set_status((obj, "Queued"))

    def open_url(self, _):
        """Opens ip address in a browser"""
        if self.check_for_none_selected():
            return
        for obj in self.main_list.GetSelectedObjects():
            url = 'http://' + obj.ip_address
            webbrowser.open_new_tab(url)

    def update_device_information(self, _):
        """Connects to device via telnet and gets serial model and firmware """
        if self.check_for_none_selected():
            return
        for obj in self.main_list.GetSelectedObjects():
            self.telnet_job_queue.put(['get_config_info', obj,
                                       self.preferences.telnet_timeout])
            self.set_status((obj, "Queued"))

    def export_to_csv(self, _):
        """Store list items in a CSV file"""
        if self.check_for_none_selected():
            return
        save_file_dialog = wx.FileDialog(
            self,
            message='Select file to add devices to or create a new file',
            defaultDir=self.storage_path,
            defaultFile="",
            wildcard="CSV files (*.csv)|*.csv",
            style=wx.FD_SAVE)
        if save_file_dialog.ShowModal() == wx.ID_OK:
            path = save_file_dialog.GetPath()
            with open(path, 'a', newline='') as store_file:
                write_csv = csv.writer(store_file, quoting=csv.QUOTE_ALL)
                for obj in self.main_list.GetSelectedObjects():
                    data = [obj.model,
                            obj.hostname,
                            obj.serial,
                            obj.firmware,
                            obj.mac_address,
                            obj.ip_address,
                            obj.ip_type,
                            obj.gateway,
                            obj.subnet,
                            obj.system]

                    write_csv.writerow(data)
                    self.set_status((obj, 'Exported'))
            self.save_main_list()

    def import_csv_file(self, _):
        """Imports a list of devices to the main list"""
        open_file_dialog = wx.FileDialog(self, message="Import a CSV file",
                                         defaultDir=self.storage_path,
                                         defaultFile="",
                                         wildcard="CSV files (*.csv)|*.csv",
                                         style=wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)
        if open_file_dialog.ShowModal() == wx.ID_OK:
            csv_path = open_file_dialog.GetPath()
            open_file_dialog.Destroy()
            dlg = wx.MessageDialog(parent=self, message='To replace all items currently in your list, click ok',
                                   caption='Replace items',
                                   style=wx.OK | wx.CANCEL)
            if dlg.ShowModal() == wx.ID_OK:
                self.main_list.DeleteAllItems()

            with open(csv_path, 'r', newline='') as csvfile:
                cvs_data = csv.reader(csvfile)
                for item in cvs_data:
                    data = datastore.NXUnit(model=item[0],
                                            hostname=item[1],
                                            serial=item[2],
                                            firmware=item[3],
                                            mac_address=item[4],
                                            ip_address=item[5],
                                            ip_type=item[6],
                                            gateway=item[7],
                                            subnet=item[8],
                                            system=item[9])

                    self.main_list.AddObject(data)
            self.save_main_list()

        else:
            open_file_dialog.Destroy()

    def import_ip_list(self, _):
        """Imports a list of IP addresses"""
        open_file_dialog = wx.FileDialog(
            self, message="Open IP List",
            defaultDir=self.storage_path,
            defaultFile="",
            wildcard="CSV files (*.csv)|*.csv",
            style=wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)

        if open_file_dialog.ShowModal() == wx.ID_OK:
            self.main_list.DeleteAllItems()
            with open(open_file_dialog.GetPath(), 'r', newline='') as csvfile:
                cvs_data = csv.reader(csvfile)
                for item in cvs_data:
                    self.main_list.AddObject(datastore.NXUnit(ip_address=item[0]))
            self.save_main_list()
            open_file_dialog.Destroy()
        else:
            open_file_dialog.Destroy()

    def import_online_tree_file(self, _):
        """Imports from an online tree report"""
        open_file_dialog = wx.FileDialog(
            self, message="Open Online Tree Report",
            defaultDir=self.storage_path,
            defaultFile="",
            wildcard="TXT files (*.txt)|*.txt",
            style=wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)

        if open_file_dialog.ShowModal() == wx.ID_OK:
            # self.olv_queue.put(['delete_all_items'])
            with open(open_file_dialog.GetPath(), 'r') as f:
                online_tree = f.read()
            online_tree_list = online_tree.split('+ IPv4 Address.......:')[1:]
            # print online_tree_list[0]
            for item in online_tree_list:
                data = datastore.NXUnit(ip_address=item.split()[0], arrival_time=datetime.datetime.now())
                self.main_list.AddObject(data)
            self.save_main_list()
            open_file_dialog.Destroy()
        else:
            open_file_dialog.Destroy()

    def generate_list(self, _):
        """Generates a list of ip addresses"""
        dia = config_menus.IpListGen(self)
        dia.ShowModal()
        dia.Destroy()

    def add_line(self, _):
        """Adds a line to the main list"""
        data = datastore.NXUnit(arrival_time=datetime.datetime.now())
        self.main_list.AddObject(data)
        self.save_main_list()

    def on_delete_item(self, _):
        """Deletes the selected item"""
        if len(self.main_list.GetSelectedObjects()) == \
           len(self.main_list.GetObjects()):
            self.main_list.DeleteAllItems()
            self.save_main_list()
            return
        if len(self.main_list.GetSelectedObjects()) == 0:
            return
        self.main_list.RemoveObjects(self.main_list.GetSelectedObjects())
        self.save_main_list()

    def on_delete_all_items(self, _):
        """Deletes all items,selected or not"""
        dlg = wx.MessageDialog(parent=self, message='Are you sure? \n This will delete all items in the list',
                               caption='Delete All Items',
                               style=wx.OK | wx.CANCEL)
        if dlg.ShowModal() == wx.ID_OK:
            self.main_list.DeleteAllItems()
            self.save_main_list()
        else:
            return

    def dhcp_on_status_bar(self, obj, incoming_time):
        self.status_bar.SetStatusText(
            incoming_time.strftime('%I:%M:%S%p') + f' -- {obj.hostname} {obj.ip_address} {obj.mac_address}')

    def incoming_dhcp(self, data):
        """Receives dhcp requests and adds them to objects to display"""
        # Check if it is filtered
        if not self.preferences.dhcp_listen:
            # print('no listen')
            return
        # Send to queue
        self.dhcp_job_queue.put(['incoming_dhcp', data])

    def save_main_list(self, event=None):
        """Saves the preference and main list"""
        self.save_config(preferences=self.preferences)

    def load_config(self):
        """Load Config"""
        try:
            with open(path.join(self.storage_path, self.storage_file), 'rb') as f:
                pick = pickle.load(f)
            return pick
        except Exception as error:
            print('unable to load plk ', error)
            # self.save_config()
            pick = {'preferences': datastore.Preferences(), 'main_list': []}
            return pick

    def save_config(self, preferences=None, gui_preferences=None):
        """Update values in config file"""
        # print('saving...')
        if preferences is None:
            # Create new config file
            preferences = datastore.Preferences()
        if not path.exists(self.storage_path):
            mkdir(self.storage_path)
        with open(path.join(self.storage_path, self.storage_file), "wb") as f:
            pick = {'preferences': preferences, 'main_list': self.main_list.GetObjects()}
            pickle.dump(pick, f)

    def telnet_missing_dia(self):
        """Show dialog for missing putty"""
        putty_path = path.join(self.storage_path, 'putty.exe')
        message = f'I\'m unable to find putty.exe in your path.\r\rClick OK to automatically download putty.exe telnet client.\r\rSaving here: {putty_path}'
        dlg = wx.MessageDialog(
            parent=self,
            message=message,
            caption='Auto download',
            style=wx.OK | wx.CANCEL)
        if dlg.ShowModal() != wx.ID_OK:
            return
        dlg.Destroy()

        # download
        putty_url = ('http://the.earth.li/~sgtatham/putty/latest/x86/putty.exe')
        try:
            with open(path.join(self.storage_path, 'putty.exe'), "wb") as f:

                response = get(putty_url, stream=True)
                total_length = response.headers.get('content-length')

                if total_length is None:  # no content length header
                    f.write(response.content)
                else:
                    dlg = wx.ProgressDialog("Downloading Putty.exe",
                                            "Progress",
                                            maximum=int(total_length),
                                            parent=self,
                                            style=wx.PD_CAN_ABORT | wx.PD_APP_MODAL | wx.PD_ELAPSED_TIME | wx.PD_REMAINING_TIME)
                    dl = 0
                    total_length = int(total_length)
                    for data in response.iter_content(chunk_size=10240):
                        dl += len(data)
                        f.write(data)
                        if not dlg.Update(dl):
                            # Canceled by user
                            break
                dlg.Destroy()
                self.preferences.set_prefs(self.storage_path)
                return
        except Exception as error:
            print('Error downloading: ', error)
            if path.exists(path.join(self.storage_path, 'putty.exe')):
                remove(path.join(self.storage_path, 'putty.exe'))
            try:
                dlg.Destroy()
            except Exception as error:
                print('Error: ', error)
        dlg = wx.MessageDialog(
            parent=self,
            message=(f'Unable to download putty.exe\r\rPlease manually copy putty.exe to {self.storage_path}\r\rThis will allow you to telnet to a device.'),
            caption='No telnet client',
            style=wx.OK)

        dlg.ShowModal()
        dlg.Destroy()

    def configure_prefs(self, _):
        """Sets user Preferences"""
        dia = config_menus.PreferencesConfig(self)
        dia.ShowModal()
        dia.Destroy()
        self.set_selected_columns()

    def on_close(self, event):
        self.save_main_list()
        dispatcher.send(signal="Shutdown")
        event.Skip()

    def on_exit(self, event):
        self.save_main_list()
        dispatcher.send(signal="Shutdown")
        self.Close()

    def on_documentation(self, event):
        webbrowser.open(path.join("docs", "NX Security Setter.pdf"))

    def on_about_box(self, _):
        """Show the About information"""

        description = """NX Security Setter is an tool for configuring security
settings on AMX NX Controllers. Features include a DHCP
monitor, import and export csv files, batch ip listing,
serial number extraction, reboots and more.
"""

        licence = """The MIT License (MIT)

Copyright (c) 2019 Jim Maciejewski

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE."""

        info = wx.adv.AboutDialogInfo()
        info.SetName(self.name)
        info.SetVersion(self.version)
        info.SetDescription(description)
        info.SetLicence(licence)
        info.AddDeveloper('Jim Maciejewski')
        wx.adv.AboutBox(info)

    def on_beer_box(self, _):
        """ Buy me a beer! Yea!"""
        dlg = wx.MessageDialog(parent=self, message='If you enjoy this program \n Learn how you can help out',
                               caption='Buy me a beer',
                               style=wx.OK)
        if dlg.ShowModal() == wx.ID_OK:
            url = 'http://ornear.com/give_a_beer'
            webbrowser.open_new_tab(url)
        dlg.Destroy()


def main():
    """run the main program"""
    nx_secuirty_setter = wx.App()  # redirect=True, filename="log.txt")
    # do processing/initialization here and create main window
    nss_frame = NX_Security_Setter_Frame(None)
    nss_frame.Show()
    nx_secuirty_setter.MainLoop()


if __name__ == '__main__':
    main()
