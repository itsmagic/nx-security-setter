""" Websockets to devices to preform tasks"""
from pydispatch import dispatcher
from threading import Thread
import ssl
import json
from websocket import create_connection
import wx
from ftplib import FTP

class Websocketjobs(Thread):

    def __init__(self, parent, queue):
        Thread.__init__(self)
        self.queue = queue
        self.parent = parent
        self.server_running = False

    def run(self):
        while True:
            job = self.queue.get()
            getattr(self, job[0])(job)
            self.queue.task_done()

    def connect_websocket(self, ip_address, ws_type, sessionid=''):
        """Creates the websocket connection"""
        ws = create_connection(f'wss://{ip_address}/web/{ws_type}',
                               cookie=f'JSESSIONID={sessionid}',
                               sslopt={'cert_reqs': ssl.CERT_NONE, 'check_hostname': False},
                               origin='https://' + ip_address)
        return ws

    def auth_login_websocket(self, username, password, ws):
        """Authenticates ws"""
        data = {'data': [{'login': {'ni_username': {'value': username}, 'ni_password': {'value': password}}}],
                'action': 'exe',
                'rsp': []}

        # ws.send('{"data":[{"login":{"ni_username":{"value":' + username + '},"ni_password":{"value":' + password + '}}}],"action":"exe","rsp":[]}')
        ws.send(json.dumps(data))
        result = ws.recv()
        obj = json.loads(result)
        if obj['login']['value'] == 1:
            sessionid = obj['auth']['sessionid']
            return sessionid
        else:
            return False

    def custom_websocket_script(self, job):
        """Do all the stuff in the script"""
        obj = job[1]
        script_path = job[2]
        try:
            self.set_status(obj, 'Connecting')
            ws = self.connect_websocket(obj.ip_address, 'login')
            sessionid = self.auth_login_websocket(self.parent.preferences.username, self.parent.preferences.get_password(), ws)
        except Exception as error:
            self.set_status(obj, error)
            return

        if not sessionid:
            self.set_status(obj, 'Login Failed')
            return
        else:
            ws.close()
            ws = self.connect_websocket(obj.ip_address, 'master', sessionid)
            failed_commands = []
            try:
                with open(script_path, 'r') as steps:
                    lines = steps.readlines()
                    for i, line in enumerate(lines):
                        if line[:1] == ' ' or line[:1] == '#' or not line.strip():
                            continue
                        current_line = json.loads(line)
                        if 'action' in current_line.keys():
                            if current_line['action'] == 'exe':
                                self.set_status(obj, 'Sending line ' + str(i + 1))
                                ws.send(line)
                                continue
                        if 'data' in current_line.keys():
                            self.set_status(obj, 'Sending line ' + str(i + 1))
                            ws.send(line)
                            result = json.loads(ws.recv())
                            status = result.pop('status', None)
                            if status is not None:
                                self.status_message(obj, status)
                            result.pop('auth', None)
                            expected_result = json.loads(lines[i + 1])
                            expected_result.pop('auth', None)
                            expected_result.pop('status', None)
                            if result == expected_result:
                                pass
                            else:
                                failed_commands.append(str(i + 1))

            except Exception as error:
                dlg = wx.MessageDialog(parent=self.parent, message='An error occured while reading script: \n ' + str(script_path) + '\n\n' + str(error), caption='Script Error', style=wx.OK)
                dlg.ShowModal()
                failed_commands.append(str(i + 1))

            ws.close()
            if failed_commands != []:
                self.set_status(obj, 'Failed Commands ' + str(failed_commands))
            else:
                self.set_status(obj, 'Success')
            return

    def send_cert(self, job):
        """Addes a cert to a NX master"""
        obj = job[1]
        cert_path = job[2]
        try:
            self.set_status(obj, 'Connecting')
            ftp = FTP(obj.ip_address, self.parent.preferences.username, self.parent.preferences.get_password())
            self.set_status(obj, 'Logged in')
            try:
                ftp.mkd('certs')
            except:
                pass

            self.set_status(obj, 'Sending certificate')
            with open(cert_path, 'rb') as cert_file:
                ftp.storbinary('STOR /certs/ldap_ad.pem', cert_file)
            ftp.close()
            self.set_status(obj, 'Success')
        except Exception as error:
            self.set_status(obj, error)

    def set_status(self, obj, status):
        """Updates progress in main"""
        data = (obj, status)
        dispatcher.send(signal='Status Update', sender=data)

    def status_message(self, obj, status):
        """Sends a pop up with status message"""
        data = (obj, status)
        dispatcher.send(signal='Status Message', sender=data)

    def notify_send_command_window(self, obj):
        """updates send_command window"""
        dispatcher.send(signal='Update Window', sender=obj)

    def error_processing(self, obj, error):
        """Send notification of error to main"""
        if str(error) == 'Not an AMX device':
            data = (obj, 'Warning, not a recognized dxlink device')
        else:
            data = (obj, str(error))
        dispatcher.send(signal='Status Update', sender=data)
