import datetime
import os
from dataclasses import dataclass, field
from shutil import which

from cryptography.fernet import Fernet

ENCRYPTION_KEY = b'm9uiRZ5qxIfloAqNCxSWc25ssQboDmDl3XZ308caMXo='
cipher_suite = Fernet(ENCRYPTION_KEY)

class NXUnit:

    def __init__(self, model='', hostname='', serial='', firmware='', device='', mac_address='',
                 ip_address='', arrival_time=datetime.datetime.now(), ip_type='', gateway='',
                 subnet='', master='', system='', status='', last_status=datetime.datetime.now()):

        self.model = model
        self.hostname = hostname
        self.serial = serial
        self.firmware = firmware
        self.mac_address = mac_address
        self.ip_address = ip_address
        self.arrival_time = arrival_time
        self.ip_type = ip_type
        self.gateway = gateway
        self.subnet = subnet
        self.system = system
        self.status = status
        self.last_status = last_status


@dataclass
class Preferences:
    username: str = 'administrator'
    password: str = b'gAAAAABe8EJPMgeHRPO_ca4s1K3KNXRkb_806Nmy1wLF6EefbJIARLxu12lLRu8HtCIAxZeRQk3YfgrErjJin79SMNMnWLfA9Q=='  # password
    number_of_threads: int = 20
    telnet_client: str = None
    telnet_timeout: int = 20
    dhcp_listen: bool = True
    amx_only_filter: bool = False
    subnet_filter: str = ''
    subnet_filter_enable: bool = False
    play_sounds: bool = True
    randomize_sounds: bool = False
    check_for_updates: bool = True
    debug: bool = False
    cols_selected: list = field(default_factory=lambda: ['Time', 'Model', 'MAC', 'IP', 'Hostname', 'Serial',
                                                         'Firmware', 'Static', 'System', 'Status'])

    def set_password(self, password):
        self.password = cipher_suite.encrypt(password.encode())

    def get_password(self):
        try:
            return cipher_suite.decrypt(self.password).decode()
        except Exception:
            return ''

    def set_prefs(self, storage_path):
        self.telnet_client = which('putty.exe')
        if self.telnet_client is None:
            # Check if we have a copy locally
            if os.path.exists(os.path.join(storage_path, 'putty.exe')):
                self.telnet_client = os.path.join(storage_path, 'putty.exe')
