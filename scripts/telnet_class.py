""" Telnets to devices to preform tasks"""

import telnetlib
from pydispatch import dispatcher
from threading import Thread
import datetime
import subprocess
import time

class Telnetjobs(Thread):

    def __init__(self, parent, queue):
        Thread.__init__(self)
        self.queue = queue
        self.parent = parent

    def run(self):
        while True:
            # gets the job from the queue
            job = self.queue.get()
            getattr(self, job[0])(job)

            # send a signal to the queue that the job is done
            self.queue.task_done()

########################################################################
    def establish_telnet(self, ip_address):
        """Creates the telnet instance"""
        telnet_session = telnetlib.Telnet(ip_address, 23, 5)
        telnet_session.set_option_negotiation_callback(self.call_back)
        return telnet_session

    def call_back(self, sock, cmd, opt):
        """ Turns on server side echoing"""
        if opt == telnetlib.ECHO and cmd in (telnetlib.WILL, telnetlib.WONT):
            sock.sendall(telnetlib.IAC + telnetlib.DO + telnetlib.ECHO)

    def get_config_info(self, job):
        """Gets serial number, firmware from device"""
        obj = job[1]
        timeout = int(job[2])
        self.set_status(obj, 'Connecting')
        try:
            telnet_session = self.establish_telnet(obj.ip_address)
            need_to_login, intro = self.check_for_login(telnet_session)
            if need_to_login:
                intro = self.login(telnet_session, self.parent.preferences.username, self.parent.preferences.get_password())
                if intro is False:
                    self.set_status(obj, 'Login Failed')
                    return
            obj.model = intro[2].decode()
            obj.firmware = intro[3].decode()
            telnet_session.write(b'show system \r')
            telnet_session.read_until(b'Serial=', timeout)
            serial = telnet_session.read_until(b'\r', timeout)
            obj.serial = serial.split(b",")[0].decode().strip().strip('\"').strip('\'')
            # serial = telnet_session.read_until(b'>', timeout)
            # print(repr(serial.decode().split(',')[0].split("'")[1]))
            # obj.serial = telnet_session.read_until(b'>', timeout).decode().split(',')[0].split("'")[1]
            telnet_session.write(b'get ip \r')
            telnet_session.read_until(b'HostName', timeout)
            ip_host = telnet_session.read_until(b'Type').decode().split()
            if len(ip_host) == 1:
                obj.hostname = ''
            else:
                obj.hostname = ' '.join(ip_host[:-1])
            ip_type = telnet_session.read_until(b'IP').split()
            if ip_type[0] == b'Static':
                obj.ip_type = 's'
            if ip_type[0] == b'DHCP':
                obj.ip_type = 'd'
            telnet_session.read_until(b'Mask')
            ip_subnet = telnet_session.read_until(b'IP').split()
            obj.subnet = ip_subnet[-2].decode()
            telnet_session.read_until(b'IP')
            ip_gateway = telnet_session.read_until(b'IP').split()
            obj.gateway = ip_gateway[-2].decode()
            telnet_session.read_until(b'MAC Address')
            ip_mac = telnet_session.read_until(b'Lease', timeout).split()
            obj.mac_address = ip_mac[0].decode()
            telnet_session.write(b'exit\r')
            telnet_session.close()
            self.set_status(obj, 'Success')
        except (IOError, Exception) as error:
            self.error_processing(obj, error)

    def check_for_login(self, telnet_session):
        """Checks if we need to login and returns the header"""
        intro = telnet_session.read_some()
        if intro.split()[0] == b'Login':
            need_to_login = True
        else:
            need_to_login = False
        return need_to_login, intro.split()

    def login(self, telnet_session, username, password):
        """Log in when required"""
        try:
            telnet_session.write(f'{username}\r'.encode())
            telnet_session.read_until(b'Password :')
            telnet_session.write(f'{password}\r'.encode())
            intro = telnet_session.read_until(b'>', 5).split()[1:]
            if intro[0] != b'Welcome':
                return False
            return intro
        except Exception as error:
            print('during login: ', error)
            return False

    def reboot(self, job):

        obj = job[1]
        self.set_status(obj, "Connecting")
        try:
            telnet_session = self.establish_telnet(obj.ip_address)
            need_to_login, intro = self.check_for_login(telnet_session)
            if need_to_login:
                intro = self.login(telnet_session, self.parent.preferences.username, self.parent.preferences.get_password())
                if intro is False:
                    self.set_status(obj, 'Login Failed')
                    return
            telnet_session.write(b'reboot\r')
            telnet_session.read_until(b'Cold', int(job[2]))
            telnet_session.close()

            self.set_status(obj, "Success")

        except Exception as error:
            self.error_processing(obj, error)

    def ping(self, job):
        """Ping devices constantly for troubleshooting"""
        obj = job[1]
        startupinfo = subprocess.STARTUPINFO()
        startupinfo.dwFlags |= subprocess.STARTF_USESHOWWINDOW
        ping = subprocess.Popen(['ping', obj.ip_address, '-t'], shell=False,
                                stdout=subprocess.PIPE, startupinfo=startupinfo)
        while self.parent.ping_active:
            for line in iter(ping.stdout.readline, ''):
                result = line.rstrip()
                if len(result) < 10:
                    continue
                if result == '':
                    continue
                elif result == '\n':
                    continue
                elif result[:7] == 'Pinging':
                    continue

                elif result.split()[-1] == 'unreachable.' or result == 'Request timed out.':
                    success = 'No'
                    ms_delay = "N/A"
                    data = (obj, [datetime.datetime.now(), ms_delay, success])
                    if self.parent.ping_active:
                        dispatcher.send(signal="Incoming Ping", sender=data)

                elif result.split()[-1][:3] == 'TTL':
                    temp = result.split()[-2]
                    ms_delay = ''.join([str(s) for s in temp if s.isdigit()])
                    success = 'Yes'
                    data = (obj, [datetime.datetime.now(), ms_delay, success])
                    if self.parent.ping_active:
                        dispatcher.send(signal="Incoming Ping", sender=data)
                else:
                    success = 'No'
                    ms_delay = "N/A"
                    data = (obj, [datetime.datetime.now(), ms_delay, success])
                    if self.parent.ping_active:
                        dispatcher.send(signal="Incoming Ping", sender=data)
                if not self.parent.ping_active:
                    break
        ping.kill()

    def get_connection(self, obj, session, timeout):
        """ Function to get connection information """
        session.write(b'get connection \r')
        session.read_until(b'Mode:', timeout)
        connection_info = session.read_until(b'>', timeout).split()
        if connection_info[0] == b'NDP' or connection_info[0] == b'AUTO':
            if connection_info[7] == b'(n/a)' or connection_info[3] == b'(not':
                obj.master = 'not connected'
                obj.system = '0'
            else:
                obj.master = connection_info[6].decode()
                obj.system = connection_info[3].decode()

        if connection_info[0] == b'TCP' or connection_info[0] == b'UDP':
            if connection_info[8] == b'(n/a)':
                obj.master = 'not connected'
                obj.system = '0'
            else:
                obj.master = connection_info[7].decode()
                obj.system = connection_info[4].decode()

    def set_status(self, obj, status):
        """Updates progress in main"""
        data = (obj, status)
        dispatcher.send(signal="Status Update", sender=data)

    def notify_send_command_window(self, obj):
        """updates send_command window"""
        dispatcher.send(signal="Update Window", sender=obj)

    def error_processing(self, obj, error):
        """Send notification of error to main"""

        if str(error) == 'Not an AMX device':
            data = (obj, 'Warning, not a recognized dxlink device')
        else:
            data = (obj, str(error))
        dispatcher.send(signal="Collect Errors", sender=data)
