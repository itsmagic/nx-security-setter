# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version Oct 26 2018)
## http://www.wxformbuilder.org/
##
## PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

###########################################################################
## Class NX_Security_Setter_Frame
###########################################################################

class NX_Security_Setter_Frame ( wx.Frame ):

	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = wx.EmptyString, pos = wx.DefaultPosition, size = wx.Size( 876,603 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

		bSizer1 = wx.BoxSizer( wx.VERTICAL )

		self.m_panel10 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer2 = wx.BoxSizer( wx.VERTICAL )

		self.olv_panel = wx.Panel( self.m_panel10, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		self.olv_sizer = wx.BoxSizer( wx.VERTICAL )


		self.olv_panel.SetSizer( self.olv_sizer )
		self.olv_panel.Layout()
		self.olv_sizer.Fit( self.olv_panel )
		self.rc_menu = wx.Menu()
		self.update_rc_menu = wx.MenuItem( self.rc_menu, wx.ID_ANY, u"Update device information", wx.EmptyString, wx.ITEM_NORMAL )
		self.rc_menu.Append( self.update_rc_menu )

		self.custom_script_rc_menu = wx.MenuItem( self.rc_menu, wx.ID_ANY, u"Custom Websocket Script", wx.EmptyString, wx.ITEM_NORMAL )
		self.rc_menu.Append( self.custom_script_rc_menu )

		self.ping_rc_menu = wx.MenuItem( self.rc_menu, wx.ID_ANY, u"Ping Device", wx.EmptyString, wx.ITEM_NORMAL )
		self.rc_menu.Append( self.ping_rc_menu )

		self.delete_rc_menu = wx.MenuItem( self.rc_menu, wx.ID_ANY, u"Delete", wx.EmptyString, wx.ITEM_NORMAL )
		self.rc_menu.Append( self.delete_rc_menu )

		self.telnet_rc_menu = wx.MenuItem( self.rc_menu, wx.ID_ANY, u"Telnet to Device", wx.EmptyString, wx.ITEM_NORMAL )
		self.rc_menu.Append( self.telnet_rc_menu )

		self.reboot_rc_menu = wx.MenuItem( self.rc_menu, wx.ID_ANY, u"Reboot Device", wx.EmptyString, wx.ITEM_NORMAL )
		self.rc_menu.Append( self.reboot_rc_menu )

		self.browser_rc_menu = wx.MenuItem( self.rc_menu, wx.ID_ANY, u"Open device in Web Browser", wx.EmptyString, wx.ITEM_NORMAL )
		self.rc_menu.Append( self.browser_rc_menu )

		self.olv_panel.Bind( wx.EVT_RIGHT_DOWN, self.olv_panelOnContextMenu )

		bSizer2.Add( self.olv_panel, 1, wx.EXPAND, 5 )


		self.m_panel10.SetSizer( bSizer2 )
		self.m_panel10.Layout()
		bSizer2.Fit( self.m_panel10 )
		bSizer1.Add( self.m_panel10, 1, wx.EXPAND, 5 )


		self.SetSizer( bSizer1 )
		self.Layout()
		self.m_menubar1 = wx.MenuBar( 0 )
		self.file_menu = wx.Menu()
		self.import_menu = wx.Menu()
		self.import_csv_menu = wx.MenuItem( self.import_menu, wx.ID_ANY, u"Import from a CSV", wx.EmptyString, wx.ITEM_NORMAL )
		self.import_menu.Append( self.import_csv_menu )

		self.import_ip_menu = wx.MenuItem( self.import_menu, wx.ID_ANY, u"Import IP list", wx.EmptyString, wx.ITEM_NORMAL )
		self.import_menu.Append( self.import_ip_menu )

		self.import_online_menu = wx.MenuItem( self.import_menu, wx.ID_ANY, u"Import From Online Tree File", wx.EmptyString, wx.ITEM_NORMAL )
		self.import_menu.Append( self.import_online_menu )

		self.file_menu.AppendSubMenu( self.import_menu, u"Import" )

		self.export_menu = wx.Menu()
		self.export_csv_menu = wx.MenuItem( self.export_menu, wx.ID_ANY, u"Export to a CSV File", wx.EmptyString, wx.ITEM_NORMAL )
		self.export_menu.Append( self.export_csv_menu )

		self.file_menu.AppendSubMenu( self.export_menu, u"Export" )

		self.quit_menu = wx.MenuItem( self.file_menu, wx.ID_ANY, u"Quit", wx.EmptyString, wx.ITEM_NORMAL )
		self.file_menu.Append( self.quit_menu )

		self.m_menubar1.Append( self.file_menu, u"File" )

		self.edit_menu = wx.Menu()
		self.select_menu = wx.Menu()
		self.all_menu = wx.MenuItem( self.select_menu, wx.ID_ANY, u"Select All", wx.EmptyString, wx.ITEM_NORMAL )
		self.select_menu.Append( self.all_menu )

		self.none_menu = wx.MenuItem( self.select_menu, wx.ID_ANY, u"Select None", wx.EmptyString, wx.ITEM_NORMAL )
		self.select_menu.Append( self.none_menu )

		self.edit_menu.AppendSubMenu( self.select_menu, u"Select" )

		self.preferences_menu = wx.MenuItem( self.edit_menu, wx.ID_ANY, u"Preferences", wx.EmptyString, wx.ITEM_NORMAL )
		self.edit_menu.Append( self.preferences_menu )

		self.m_menubar1.Append( self.edit_menu, u"Edit" )

		self.actions_menu = wx.Menu()
		self.update_device_menu = wx.MenuItem( self.actions_menu, wx.ID_ANY, u"Update Device Information", wx.EmptyString, wx.ITEM_NORMAL )
		self.actions_menu.Append( self.update_device_menu )

		self.telnet_menu = wx.MenuItem( self.actions_menu, wx.ID_ANY, u"Telnet to Device", wx.EmptyString, wx.ITEM_NORMAL )
		self.actions_menu.Append( self.telnet_menu )

		self.ssh_menu = wx.MenuItem( self.actions_menu, wx.ID_ANY, u"SSH to Device", wx.EmptyString, wx.ITEM_NORMAL )
		self.actions_menu.Append( self.ssh_menu )

		self.reboot_menu = wx.MenuItem( self.actions_menu, wx.ID_ANY, u"Reboot Device", wx.EmptyString, wx.ITEM_NORMAL )
		self.actions_menu.Append( self.reboot_menu )

		self.m_menubar1.Append( self.actions_menu, u"Actions" )

		self.tools_menu = wx.Menu()
		self.ping_menu = wx.MenuItem( self.tools_menu, wx.ID_ANY, u"Ping Devices", wx.EmptyString, wx.ITEM_NORMAL )
		self.tools_menu.Append( self.ping_menu )

		self.add_menu = wx.MenuItem( self.tools_menu, wx.ID_ANY, u"Add line item", wx.EmptyString, wx.ITEM_NORMAL )
		self.tools_menu.Append( self.add_menu )

		self.generate_menu = wx.MenuItem( self.tools_menu, wx.ID_ANY, u"Generate IP List", wx.EmptyString, wx.ITEM_NORMAL )
		self.tools_menu.Append( self.generate_menu )

		self.m_menubar1.Append( self.tools_menu, u"Tools" )

		self.listen_menu = wx.Menu()
		self.dhcp_sniffing_chk = wx.MenuItem( self.listen_menu, wx.ID_ANY, u"Listen for DHCP requests", wx.EmptyString, wx.ITEM_CHECK )
		self.listen_menu.Append( self.dhcp_sniffing_chk )
		self.dhcp_sniffing_chk.Check( True )

		self.amx_only_filter_chk = wx.MenuItem( self.listen_menu, wx.ID_ANY, u"Only add AMX devices", wx.EmptyString, wx.ITEM_CHECK )
		self.listen_menu.Append( self.amx_only_filter_chk )

		self.m_menubar1.Append( self.listen_menu, u"Listen" )

		self.delete_menu = wx.Menu()
		self.delete_item_menu = wx.MenuItem( self.delete_menu, wx.ID_ANY, u"Delete Item", wx.EmptyString, wx.ITEM_NORMAL )
		self.delete_menu.Append( self.delete_item_menu )

		self.delete_all_menu = wx.MenuItem( self.delete_menu, wx.ID_ANY, u"Delete All Items", wx.EmptyString, wx.ITEM_NORMAL )
		self.delete_menu.Append( self.delete_all_menu )

		self.m_menubar1.Append( self.delete_menu, u"Delete" )

		self.help_menu = wx.Menu()
		self.documentation_menu = wx.MenuItem( self.help_menu, wx.ID_ANY, u"Documentation", wx.EmptyString, wx.ITEM_NORMAL )
		self.help_menu.Append( self.documentation_menu )

		self.about_menu = wx.MenuItem( self.help_menu, wx.ID_ANY, u"About", wx.EmptyString, wx.ITEM_NORMAL )
		self.help_menu.Append( self.about_menu )

		self.m_menubar1.Append( self.help_menu, u"Help" )

		self.SetMenuBar( self.m_menubar1 )

		self.status_bar = self.CreateStatusBar( 2, wx.STB_SIZEGRIP, wx.ID_ANY )

		self.Centre( wx.BOTH )

		# Connect Events
		self.Bind( wx.EVT_CLOSE, self.on_close )
		self.Bind( wx.EVT_MENU, self.update_device_information, id = self.update_rc_menu.GetId() )
		self.Bind( wx.EVT_MENU, self.custom_websocket_script, id = self.custom_script_rc_menu.GetId() )
		self.Bind( wx.EVT_MENU, self.multi_ping, id = self.ping_rc_menu.GetId() )
		self.Bind( wx.EVT_MENU, self.on_delete_item, id = self.delete_rc_menu.GetId() )
		self.Bind( wx.EVT_MENU, self.telnet_to, id = self.telnet_rc_menu.GetId() )
		self.Bind( wx.EVT_MENU, self.reboot, id = self.reboot_rc_menu.GetId() )
		self.Bind( wx.EVT_MENU, self.open_url, id = self.browser_rc_menu.GetId() )
		self.Bind( wx.EVT_MENU, self.import_csv_file, id = self.import_csv_menu.GetId() )
		self.Bind( wx.EVT_MENU, self.import_ip_list, id = self.import_ip_menu.GetId() )
		self.Bind( wx.EVT_MENU, self.import_online_tree_file, id = self.import_online_menu.GetId() )
		self.Bind( wx.EVT_MENU, self.export_to_csv, id = self.export_csv_menu.GetId() )
		self.Bind( wx.EVT_MENU, self.on_exit, id = self.quit_menu.GetId() )
		self.Bind( wx.EVT_MENU, self.on_select_all, id = self.all_menu.GetId() )
		self.Bind( wx.EVT_MENU, self.on_select_none, id = self.none_menu.GetId() )
		self.Bind( wx.EVT_MENU, self.configure_prefs, id = self.preferences_menu.GetId() )
		self.Bind( wx.EVT_MENU, self.update_device_information, id = self.update_device_menu.GetId() )
		self.Bind( wx.EVT_MENU, self.telnet_to, id = self.telnet_menu.GetId() )
		self.Bind( wx.EVT_MENU, self.ssh_to, id = self.ssh_menu.GetId() )
		self.Bind( wx.EVT_MENU, self.reboot, id = self.reboot_menu.GetId() )
		self.Bind( wx.EVT_MENU, self.multi_ping, id = self.ping_menu.GetId() )
		self.Bind( wx.EVT_MENU, self.add_line, id = self.add_menu.GetId() )
		self.Bind( wx.EVT_MENU, self.generate_list, id = self.generate_menu.GetId() )
		self.Bind( wx.EVT_MENU, self.on_dhcp_sniffing, id = self.dhcp_sniffing_chk.GetId() )
		self.Bind( wx.EVT_MENU, self.on_amx_only_filter, id = self.amx_only_filter_chk.GetId() )
		self.Bind( wx.EVT_MENU, self.on_delete_item, id = self.delete_item_menu.GetId() )
		self.Bind( wx.EVT_MENU, self.on_delete_all_items, id = self.delete_all_menu.GetId() )
		self.Bind( wx.EVT_MENU, self.on_documentation, id = self.documentation_menu.GetId() )
		self.Bind( wx.EVT_MENU, self.on_about_box, id = self.about_menu.GetId() )

	def __del__( self ):
		pass


	# Virtual event handlers, overide them in your derived class
	def on_close( self, event ):
		event.Skip()

	def update_device_information( self, event ):
		event.Skip()

	def custom_websocket_script( self, event ):
		event.Skip()

	def multi_ping( self, event ):
		event.Skip()

	def on_delete_item( self, event ):
		event.Skip()

	def telnet_to( self, event ):
		event.Skip()

	def reboot( self, event ):
		event.Skip()

	def open_url( self, event ):
		event.Skip()

	def import_csv_file( self, event ):
		event.Skip()

	def import_ip_list( self, event ):
		event.Skip()

	def import_online_tree_file( self, event ):
		event.Skip()

	def export_to_csv( self, event ):
		event.Skip()

	def on_exit( self, event ):
		event.Skip()

	def on_select_all( self, event ):
		event.Skip()

	def on_select_none( self, event ):
		event.Skip()

	def configure_prefs( self, event ):
		event.Skip()



	def ssh_to( self, event ):
		event.Skip()



	def add_line( self, event ):
		event.Skip()

	def generate_list( self, event ):
		event.Skip()

	def on_dhcp_sniffing( self, event ):
		event.Skip()

	def on_amx_only_filter( self, event ):
		event.Skip()


	def on_delete_all_items( self, event ):
		event.Skip()

	def on_about_box( self, event ):
		event.Skip()

	def olv_panelOnContextMenu( self, event ):
		self.olv_panel.PopupMenu( self.rc_menu, event.GetPoint() )


###########################################################################
## Class PingDetail
###########################################################################

class PingDetail ( wx.Dialog ):

	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"Details view", pos = wx.DefaultPosition, size = wx.Size( 390,550 ), style = wx.DEFAULT_DIALOG_STYLE|wx.RESIZE_BORDER )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

		bSizer21 = wx.BoxSizer( wx.VERTICAL )

		self.olv_panel = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer24 = wx.BoxSizer( wx.VERTICAL )

		self.olv_sizer = wx.BoxSizer( wx.VERTICAL )


		bSizer24.Add( self.olv_sizer, 1, wx.EXPAND, 5 )

		bSizer23 = wx.BoxSizer( wx.HORIZONTAL )

		bSizer26 = wx.BoxSizer( wx.HORIZONTAL )

		self.auto_update_chk = wx.CheckBox( self.olv_panel, wx.ID_ANY, u"Auto update", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer26.Add( self.auto_update_chk, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )


		bSizer23.Add( bSizer26, 1, wx.ALIGN_CENTER_VERTICAL, 5 )

		bSizer27 = wx.BoxSizer( wx.VERTICAL )

		self.m_button4 = wx.Button( self.olv_panel, wx.ID_ANY, u"Refresh", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer27.Add( self.m_button4, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )


		bSizer23.Add( bSizer27, 1, 0, 5 )


		bSizer24.Add( bSizer23, 0, wx.EXPAND, 5 )


		self.olv_panel.SetSizer( bSizer24 )
		self.olv_panel.Layout()
		bSizer24.Fit( self.olv_panel )
		bSizer21.Add( self.olv_panel, 1, wx.EXPAND, 5 )


		self.SetSizer( bSizer21 )
		self.Layout()

		self.Centre( wx.BOTH )

		# Connect Events
		self.auto_update_chk.Bind( wx.EVT_CHECKBOX, self.on_auto_update )
		self.m_button4.Bind( wx.EVT_BUTTON, self.on_refresh )

	def __del__( self ):
		pass


	# Virtual event handlers, overide them in your derived class
	def on_auto_update( self, event ):
		event.Skip()

	def on_refresh( self, event ):
		event.Skip()


###########################################################################
## Class Preferences
###########################################################################

class Preferences ( wx.Dialog ):

	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"Preferences", pos = wx.DefaultPosition, size = wx.Size( -1,-1 ), style = wx.DEFAULT_DIALOG_STYLE )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

		bSizer10 = wx.BoxSizer( wx.VERTICAL )

		sbSizer14 = wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, u"Subnet Filter" ), wx.VERTICAL )

		bSizer63 = wx.BoxSizer( wx.VERTICAL )

		self.subnet_filter_chk = wx.CheckBox( sbSizer14.GetStaticBox(), wx.ID_ANY, u"Enable Subnet Filter", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer63.Add( self.subnet_filter_chk, 0, wx.ALL, 5 )


		sbSizer14.Add( bSizer63, 0, wx.EXPAND, 5 )

		bSizer64 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText31 = wx.StaticText( sbSizer14.GetStaticBox(), wx.ID_ANY, u"CIDR", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText31.Wrap( -1 )

		bSizer64.Add( self.m_staticText31, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.subnet_filter_txt = wx.TextCtrl( sbSizer14.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.subnet_filter_txt.SetToolTip( u"Please enter the subnet you would like to monitor, using CIDR notation. For example: 192.168.71.0/255.255.255.0 or 192.168.71.0/24" )

		bSizer64.Add( self.subnet_filter_txt, 1, wx.ALL, 5 )


		sbSizer14.Add( bSizer64, 0, wx.EXPAND, 5 )


		bSizer10.Add( sbSizer14, 0, wx.ALL|wx.EXPAND, 5 )

		sbSizer2 = wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, u"Notifications" ), wx.VERTICAL )

		self.sounds_chk = wx.CheckBox( sbSizer2.GetStaticBox(), wx.ID_ANY, u"Play Sounds", wx.DefaultPosition, wx.DefaultSize, 0 )
		sbSizer2.Add( self.sounds_chk, 0, wx.ALL, 5 )

		self.funny_sounds_chk = wx.CheckBox( sbSizer2.GetStaticBox(), wx.ID_ANY, u"Randomize Sound Folder", wx.DefaultPosition, wx.DefaultSize, 0 )
		sbSizer2.Add( self.funny_sounds_chk, 0, wx.ALL, 5 )

		self.check_for_updates_chk = wx.CheckBox( sbSizer2.GetStaticBox(), wx.ID_ANY, u"Check for updates", wx.DefaultPosition, wx.DefaultSize, 0 )
		sbSizer2.Add( self.check_for_updates_chk, 0, wx.ALL, 5 )


		bSizer10.Add( sbSizer2, 0, wx.EXPAND|wx.ALL, 5 )

		sbSizer3 = wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, u"Columns To Display" ), wx.VERTICAL )

		gSizer1 = wx.GridSizer( 0, 2, 0, 0 )

		self.time_chk = wx.CheckBox( sbSizer3.GetStaticBox(), wx.ID_ANY, u"Time", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.time_chk.SetValue(True)
		self.time_chk.Enable( False )

		gSizer1.Add( self.time_chk, 0, wx.ALL, 5 )

		self.model_chk = wx.CheckBox( sbSizer3.GetStaticBox(), wx.ID_ANY, u"Model", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer1.Add( self.model_chk, 0, wx.ALL, 5 )

		self.mac_chk = wx.CheckBox( sbSizer3.GetStaticBox(), wx.ID_ANY, u"MAC", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer1.Add( self.mac_chk, 0, wx.ALL, 5 )

		self.ip_chk = wx.CheckBox( sbSizer3.GetStaticBox(), wx.ID_ANY, u"IP", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.ip_chk.SetValue(True)
		self.ip_chk.Enable( False )

		gSizer1.Add( self.ip_chk, 0, wx.ALL, 5 )

		self.hostname_chk = wx.CheckBox( sbSizer3.GetStaticBox(), wx.ID_ANY, u"Hostname", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer1.Add( self.hostname_chk, 0, wx.ALL, 5 )

		self.serial_chk = wx.CheckBox( sbSizer3.GetStaticBox(), wx.ID_ANY, u"Serial Number", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer1.Add( self.serial_chk, 0, wx.ALL, 5 )

		self.firmware_chk = wx.CheckBox( sbSizer3.GetStaticBox(), wx.ID_ANY, u"Firmware", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer1.Add( self.firmware_chk, 0, wx.ALL, 5 )

		self.static_chk = wx.CheckBox( sbSizer3.GetStaticBox(), wx.ID_ANY, u"Static", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer1.Add( self.static_chk, 0, wx.ALL, 5 )

		self.system_chk = wx.CheckBox( sbSizer3.GetStaticBox(), wx.ID_ANY, u"System", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer1.Add( self.system_chk, 0, wx.ALL, 5 )

		self.status_chk = wx.CheckBox( sbSizer3.GetStaticBox(), wx.ID_ANY, u"Status", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.status_chk.SetValue(True)
		self.status_chk.Enable( False )

		gSizer1.Add( self.status_chk, 0, wx.ALL, 5 )


		sbSizer3.Add( gSizer1, 0, wx.EXPAND, 5 )


		bSizer10.Add( sbSizer3, 0, wx.EXPAND|wx.ALL, 5 )

		sbSizer15 = wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, u"Username/Password" ), wx.VERTICAL )

		bSizer701 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText32 = wx.StaticText( sbSizer15.GetStaticBox(), wx.ID_ANY, u"Username", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText32.Wrap( -1 )

		bSizer701.Add( self.m_staticText32, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.master_user_txt = wx.TextCtrl( sbSizer15.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 200,-1 ), 0 )
		bSizer701.Add( self.master_user_txt, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		sbSizer15.Add( bSizer701, 1, wx.EXPAND, 5 )

		bSizer711 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText33 = wx.StaticText( sbSizer15.GetStaticBox(), wx.ID_ANY, u"Password", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText33.Wrap( -1 )

		bSizer711.Add( self.m_staticText33, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.master_password_txt = wx.TextCtrl( sbSizer15.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 200,-1 ), wx.TE_PASSWORD )
		bSizer711.Add( self.master_password_txt, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		sbSizer15.Add( bSizer711, 0, wx.EXPAND, 5 )


		bSizer10.Add( sbSizer15, 0, wx.EXPAND|wx.ALL, 5 )

		m_sdbSizer3 = wx.StdDialogButtonSizer()
		self.m_sdbSizer3OK = wx.Button( self, wx.ID_OK )
		m_sdbSizer3.AddButton( self.m_sdbSizer3OK )
		self.m_sdbSizer3Cancel = wx.Button( self, wx.ID_CANCEL )
		m_sdbSizer3.AddButton( self.m_sdbSizer3Cancel )
		m_sdbSizer3.Realize();

		bSizer10.Add( m_sdbSizer3, 1, wx.EXPAND|wx.ALL, 5 )


		self.SetSizer( bSizer10 )
		self.Layout()
		bSizer10.Fit( self )

		self.Centre( wx.BOTH )

		# Connect Events
		self.subnet_filter_chk.Bind( wx.EVT_CHECKBOX, self.on_subnet_enable )
		self.m_sdbSizer3Cancel.Bind( wx.EVT_BUTTON, self.on_cancel )
		self.m_sdbSizer3OK.Bind( wx.EVT_BUTTON, self.on_ok )

	def __del__( self ):
		pass


	# Virtual event handlers, overide them in your derived class
	def on_subnet_enable( self, event ):
		event.Skip()

	def on_cancel( self, event ):
		event.Skip()

	def on_ok( self, event ):
		event.Skip()


###########################################################################
## Class MultiPing
###########################################################################

class MultiPing ( wx.Dialog ):

	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = wx.EmptyString, pos = wx.DefaultPosition, size = wx.Size( 760,300 ), style = wx.DEFAULT_DIALOG_STYLE|wx.RESIZE_BORDER )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

		bSizer4 = wx.BoxSizer( wx.VERTICAL )

		bSizer44 = wx.BoxSizer( wx.VERTICAL )

		self.olv_panel = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		self.olv_sizer = wx.BoxSizer( wx.VERTICAL )


		self.olv_panel.SetSizer( self.olv_sizer )
		self.olv_panel.Layout()
		self.olv_sizer.Fit( self.olv_panel )
		bSizer44.Add( self.olv_panel, 1, wx.EXPAND |wx.ALL, 5 )


		bSizer4.Add( bSizer44, 1, wx.EXPAND, 5 )

		bSizer45 = wx.BoxSizer( wx.HORIZONTAL )

		sbSizer8 = wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, u"Logging" ), wx.HORIZONTAL )

		bSizer65 = wx.BoxSizer( wx.VERTICAL )

		bSizer67 = wx.BoxSizer( wx.HORIZONTAL )

		self.log_enable_chk = wx.CheckBox( sbSizer8.GetStaticBox(), wx.ID_ANY, u"Log to file", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer67.Add( self.log_enable_chk, 0, wx.ALL, 5 )

		self.log_path_txt = wx.StaticText( sbSizer8.GetStaticBox(), wx.ID_ANY, u"MagicDXLinkConfigruator", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.log_path_txt.Wrap( -1 )

		bSizer67.Add( self.log_path_txt, 0, wx.ALL, 5 )


		bSizer65.Add( bSizer67, 1, wx.EXPAND, 5 )


		sbSizer8.Add( bSizer65, 1, wx.EXPAND, 5 )


		bSizer45.Add( sbSizer8, 0, wx.EXPAND|wx.ALL, 5 )

		bSizer66 = wx.BoxSizer( wx.VERTICAL )

		self.m_button10 = wx.Button( self, wx.ID_ANY, u"Reset Selected", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer66.Add( self.m_button10, 0, wx.ALL|wx.ALIGN_RIGHT, 5 )


		bSizer45.Add( bSizer66, 1, wx.ALIGN_BOTTOM|wx.ALL, 5 )


		bSizer4.Add( bSizer45, 0, wx.EXPAND, 5 )


		self.SetSizer( bSizer4 )
		self.Layout()
		self.rc_menu = wx.Menu()
		self.details_rc_menu = wx.MenuItem( self.rc_menu, wx.ID_ANY, u"Show Details", wx.EmptyString, wx.ITEM_NORMAL )
		self.rc_menu.Append( self.details_rc_menu )

		self.reset_rc = wx.MenuItem( self.rc_menu, wx.ID_ANY, u"Reset Count", wx.EmptyString, wx.ITEM_NORMAL )
		self.rc_menu.Append( self.reset_rc )

		self.delete_rc_menu = wx.MenuItem( self.rc_menu, wx.ID_ANY, u"Delete", wx.EmptyString, wx.ITEM_NORMAL )
		self.rc_menu.Append( self.delete_rc_menu )

		self.Bind( wx.EVT_RIGHT_DOWN, self.MultiPingOnContextMenu )


		self.Centre( wx.BOTH )

		# Connect Events
		self.log_enable_chk.Bind( wx.EVT_CHECKBOX, self.on_log_enable )
		self.m_button10.Bind( wx.EVT_BUTTON, self.on_reset )
		self.Bind( wx.EVT_MENU, self.on_show_details, id = self.details_rc_menu.GetId() )
		self.Bind( wx.EVT_MENU, self.on_reset, id = self.reset_rc.GetId() )
		self.Bind( wx.EVT_MENU, self.on_delete, id = self.delete_rc_menu.GetId() )

	def __del__( self ):
		pass


	# Virtual event handlers, overide them in your derived class
	def on_log_enable( self, event ):
		event.Skip()

	def on_reset( self, event ):
		event.Skip()

	def on_show_details( self, event ):
		event.Skip()


	def on_delete( self, event ):
		event.Skip()

	def MultiPingOnContextMenu( self, event ):
		self.PopupMenu( self.rc_menu, event.GetPoint() )


###########################################################################
## Class GenerateIP
###########################################################################

class GenerateIP ( wx.Dialog ):

	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"Generate IP list", pos = wx.DefaultPosition, size = wx.DefaultSize, style = wx.DEFAULT_DIALOG_STYLE )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

		bSizer27 = wx.BoxSizer( wx.VERTICAL )

		self.m_panel4 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer28 = wx.BoxSizer( wx.VERTICAL )

		bSizer29 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText9 = wx.StaticText( self.m_panel4, wx.ID_ANY, u"Starting IP", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTER_HORIZONTAL )
		self.m_staticText9.Wrap( -1 )

		bSizer29.Add( self.m_staticText9, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.start_txt = wx.TextCtrl( self.m_panel4, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.start_txt.SetMinSize( wx.Size( 200,-1 ) )

		bSizer29.Add( self.start_txt, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		bSizer28.Add( bSizer29, 1, wx.EXPAND, 5 )

		bSizer30 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText10 = wx.StaticText( self.m_panel4, wx.ID_ANY, u"Finishing IP", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTER_HORIZONTAL )
		self.m_staticText10.Wrap( -1 )

		bSizer30.Add( self.m_staticText10, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.finish_txt = wx.TextCtrl( self.m_panel4, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.finish_txt.SetMinSize( wx.Size( 200,-1 ) )

		bSizer30.Add( self.finish_txt, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		bSizer28.Add( bSizer30, 1, wx.EXPAND, 5 )

		bSizer31 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_button5 = wx.Button( self.m_panel4, wx.ID_ANY, u"Replace List", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer31.Add( self.m_button5, 0, wx.ALL, 5 )

		self.m_button6 = wx.Button( self.m_panel4, wx.ID_ANY, u"Add to List", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer31.Add( self.m_button6, 0, wx.ALL, 5 )

		self.m_button7 = wx.Button( self.m_panel4, wx.ID_ANY, u"Save as File", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer31.Add( self.m_button7, 0, wx.ALL, 5 )


		bSizer28.Add( bSizer31, 1, wx.EXPAND|wx.ALIGN_CENTER_HORIZONTAL, 5 )


		self.m_panel4.SetSizer( bSizer28 )
		self.m_panel4.Layout()
		bSizer28.Fit( self.m_panel4 )
		bSizer27.Add( self.m_panel4, 1, wx.EXPAND |wx.ALL, 5 )


		self.SetSizer( bSizer27 )
		self.Layout()
		bSizer27.Fit( self )

		self.Centre( wx.BOTH )

		# Connect Events
		self.m_button5.Bind( wx.EVT_BUTTON, self.on_action )
		self.m_button6.Bind( wx.EVT_BUTTON, self.on_action )
		self.m_button7.Bind( wx.EVT_BUTTON, self.on_save )

	def __del__( self ):
		pass


	# Virtual event handlers, overide them in your derived class
	def on_action( self, event ):
		event.Skip()


	def on_save( self, event ):
		event.Skip()


