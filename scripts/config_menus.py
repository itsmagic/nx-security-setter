"""Menus for Magic DXLink Configurator"""

import wx
import csv
from scripts import nss_gui, datastore
from netaddr import IPRange, IPNetwork


class PreferencesConfig(nss_gui.Preferences):
    """Sets the preferences """

    def __init__(self, parent):
        nss_gui.Preferences.__init__(self, parent)

        self.parent = parent
        self.prefs = self.parent.preferences
        self.set_values()

    def set_values(self):
        """Set the field values"""

        self.subnet_filter_txt.SetValue(self.prefs.subnet_filter)
        self.subnet_filter_txt.Enable(self.prefs.subnet_filter_enable)
        self.subnet_filter_chk.SetValue(self.prefs.subnet_filter_enable)

        self.sounds_chk.SetValue(int(self.prefs.play_sounds))
        self.funny_sounds_chk.SetValue(int(self.prefs.randomize_sounds))
        self.check_for_updates_chk.SetValue(int(self.prefs.check_for_updates))

        self.master_user_txt.SetValue(self.prefs.username)
        self.master_password_txt.SetValue(self.prefs.get_password())

        for item in self.prefs.cols_selected:
            getattr(self, item.lower() + '_chk').SetValue(True)

    def on_ok(self, _):
        """When user clicks ok"""
        self.prefs.cols_selected = []
        columns = ['Time', 'Model', 'MAC', 'IP', 'Hostname', 'Serial', 'Firmware', 'Static', 'System', 'Status']
        for item in columns:
            if getattr(self, item.lower() + '_chk').GetValue():
                self.prefs.cols_selected.append(item)
        self.prefs.subnet_filter_enable = self.subnet_filter_chk.GetValue()
        if self.subnet_filter_chk.GetValue():
            try:
                IPNetwork(self.subnet_filter_txt.GetValue())
            except Exception as error:
                self.bad_subnet(error)
                return
            self.prefs.subnet_filter = self.subnet_filter_txt.GetValue()

        self.prefs.play_sounds = self.sounds_chk.GetValue()
        self.prefs.randomize_sounds = self.funny_sounds_chk.GetValue()
        self.prefs.check_for_updates = self.check_for_updates_chk.GetValue()

        if self.master_user_txt.GetValue() != "":
            self.prefs.username = self.master_user_txt.GetValue()
        if self.master_password_txt.GetValue() != "":
            self.prefs.set_password(self.master_password_txt.GetValue())

        self.Destroy()

    def bad_subnet(self, error):
        """Dialog showing error"""
        dlg = wx.MessageDialog(parent=self,
                               message=f'The subnet you have entered is invalid.\n\n{str(error)}\nCIDR example 192.168.71.0/24 or 192.168.71.0/255.255.255.0',
                               caption='Error saving subnet',
                               style=wx.OK)
        dlg.ShowModal()
        self.subnet_filter_chk.SetValue(False)
        self.subnet_filter_txt.Enable(False)

    def on_subnet_enable(self, event):
        """Enable or disable the subnet field"""
        self.subnet_filter_txt.Enable(self.subnet_filter_chk.GetValue())

    def on_cancel(self, _):
        """When user clicks cancel"""
        self.Destroy()


class IpListGen(nss_gui.GenerateIP):
    """Generates a list of IP's"""

    def __init__(self, parent):
        nss_gui.GenerateIP.__init__(self, parent)

        self.parent = parent
        self.prefs = self.parent.preferences

        self.start_txt.SetLabel('127.0.0.1')
        self.finish_txt.SetLabel('127.0.0.255')

        self.data = []

        self.SetTitle("Generate IP list")

    def on_action(self, event):
        """Testing event"""
        if not self.gen_list():
            return
        if self.check_size():
            if event.GetEventObject().GetLabel() == "Add to List":
                self.on_add()
            elif event.GetEventObject().GetLabel() == "Replace List":
                self.on_replace()
        else:
            return

    def on_replace(self):
        """Replaces list with generated list"""
        if not self.gen_list():
            return
        self.parent.main_list.DeleteAllItems()
        self.on_add()

    def on_add(self):
        """Adds to the bottom of the list"""
        if not self.gen_list():
            return
        for item in self.data:
            obj = datastore.NXUnit()
            obj.ip_address = item
            self.parent.main_list.AddObject(obj)
        self.parent.save_main_list()
        self.Destroy()

    def on_save(self, _):
        """Saves the ip list to a file"""
        if not self.gen_list():
            return
        if self.check_size():
            save_file_dialog = wx.FileDialog(self, message="Save IP list",
                                             defaultDir=self.parent.storage_path,
                                             defaultFile="generatediplist.csv",
                                             wildcard="CSV files (*.csv)|*.csv",
                                             style=wx.FD_SAVE)
            if save_file_dialog.ShowModal() == wx.ID_OK:

                path = save_file_dialog.GetPath()
                with open(path, 'w', newline='') as ip_list_file:
                    writer_csv = csv.writer(ip_list_file)
                    for item in self.data:
                        writer_csv.writerow([item])
                    self.Destroy()
            else:
                self.Destroy()
        else:
            return

    def gen_list(self):
        """Generates the IP list"""
        self.data = []
        try:
            ip_range = IPRange(self.start_txt.GetValue(),
                               self.finish_txt.GetValue())
        except Exception as error:
            print('error: ', error)
            dlg = wx.MessageDialog(parent=self,
                                   message=f'The IP address entered is invalid\r{error}',
                                   caption='Invalid IP',
                                   style=wx.OK)
            dlg.ShowModal()
            return False
        for address in list(ip_range):
            self.data.append(str(address))
        return True

    def check_size(self):
        """Warning abou the size of the range"""
        if len(self.data) > 500:
            dlg = wx.MessageDialog(
                parent=self,
                message='Are you sure? \n\nThis will create ' +
                        str(len(self.data)) +
                        ' IP\'s. Creating more than 1000 IP\'s could slow' +
                        ' or even crash the program',
                        caption='Creating IP list',
                        style=wx.OK | wx.CANCEL)
            if dlg.ShowModal() == wx.ID_OK:
                return True
            else:
                return False
        else:
            return True

